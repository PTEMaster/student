//
//  AppDelegate.swift
//  SwipeStudent
//
//  Created by mac on 19/05/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import UserNotifications
import MBProgressHUD
import Reachability
import AWSSNS
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var isNetworkBecomeAvailableFirstTime = false
    var reachability: Reachability?
    var window: UIWindow?
    var strDeviceToken: String = ""
    var strUserId = ""
    var strCauseName = ""
    var strEndPointAWS = ""
    var isLoginTrue : Bool = false
    var strPersonId = ""
    let SNSPlatformApplicationArn = "arn:aws:sns:us-east-1:460018693145:app/APNS/StudentLiveSwipe"
    
    //var pinpoint: AWSPinpoint?
    // CTI :- com.swipek12.SwipeStudent
    // client bu :- com.swipek12.idcard
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        registerForPushNotifications()

        //MARK:- Chnage key AWS
        
        /// Setup AWS Cognito credentials
        let credentialsProvider = AWSCognitoCredentialsProvider(
            regionType: AWSRegionType.USEast1, identityPoolId: "us-east-1:a52c6584-8790-40cf-8370-41088b6e9c2e")
        let defaultServiceConfiguration = AWSServiceConfiguration(
            region: AWSRegionType.USEast1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = defaultServiceConfiguration
        
        /** start code copy **/
        // Create AWSMobileClient to connect with AWS
        //        AWSMobileClient.sharedInstance().initialize { (userState, error) in
        //            if let error = error {
        //                print("Error initializing AWSMobileClient: \(error.localizedDescription)")
        //            } else if let userState = userState {
        //                print("AWSMobileClient initialized. Current UserState: \(userState.rawValue)")
        //            }
        //        }
        //
        //        // Initialize Pinpoint
        //        let pinpointConfiguration = AWSPinpointConfiguration.defaultPinpointConfiguration(launchOptions: launchOptions)
        //        pinpoint = AWSPinpoint(configuration: pinpointConfiguration)
        //        /** end code copy **/
        //
        //        registerForPushNotifications()
        
        // Start reachability without a hostname intially
        setupReachability(nil, useClosures: false)
        startNotifier()
        FirebaseApp.configure()
        // Configure |IQKeyboardManager|
        IQKeyboardManager.shared.enable = true;
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        return true
    }
    
    // Other imports...
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        /// Attach the device token to the user defaults
        var token = ""
        
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        strDeviceToken = token
        
        UserDefaults.standard.set(token, forKey: "deviceTokenForSNS")
        /// Create a platform endpoint. In this case, the endpoint is a
        /// device endpoint ARN
        let sns = AWSSNS.default()
        let request = AWSSNSCreatePlatformEndpointInput()
        request?.token = token
        request?.platformApplicationArn = SNSPlatformApplicationArn
        sns.createPlatformEndpoint(request!).continueWith(executor: AWSExecutor.mainThread(), block: { (task: AWSTask!) -> AnyObject? in
            if task.error != nil {
                print("Error: \(String(describing: task.error))")
            } else {
                let createEndpointResponse = task.result! as AWSSNSCreateEndpointResponse
                if let endpointArnForSNS = createEndpointResponse.endpointArn {
                    print("endpointArn: \(endpointArnForSNS)")
                    self.strEndPointAWS = endpointArnForSNS
                    UserDefaults.standard.set(endpointArnForSNS, forKey: "endpointArnForSNS")
                }
            }
            return nil
        })
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func registerForPushNotifications(application: UIApplication) {
        /// The notifications settings
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    UIApplication.shared.registerForRemoteNotifications()
                } else {
                    //Do stuff if unsuccessful…
                }
            })
        } else {
            let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
    }
    
    // Called when a notification is delivered to a foreground app.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if let value = UserDefaults.standard.string(forKey: "Loggedin") {
            if value == "true" {
                print("User Info = ",notification.request.content.userInfo)
                completionHandler([.alert, .badge, .sound])
            }
        }
    }
    
    // Called to let your app know which action was selected by the user for a given notification.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let value = UserDefaults.standard.string(forKey: "Loggedin") {
            if value == "true" {
                print("User Info = ",response.notification.request.content.userInfo)
                completionHandler()
            }
        }
    }
    
    
    //    func application(
    //        _ application: UIApplication,
    //        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //
    //        pinpoint!.notificationManager.interceptDidRegisterForRemoteNotifications(
    //            withDeviceToken: deviceToken)
    //    }
    //
    //    func application(
    //        _ application: UIApplication,
    //        didReceiveRemoteNotification userInfo: [AnyHashable: Any],
    //        fetchCompletionHandler completionHandler:
    //        @escaping (UIBackgroundFetchResult) -> Void) {
    //
    //        pinpoint!.notificationManager.interceptDidReceiveRemoteNotification(
    //            userInfo, fetchCompletionHandler: completionHandler)
    //
    //        if (application.applicationState == .active) {
    //            let alert = UIAlertController(title: "Notification Received",
    //                                          message: userInfo.description,
    //                                          preferredStyle: .alert)
    //            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    //
    //            UIApplication.shared.keyWindow?.rootViewController?.present(
    //                alert, animated: true, completion:nil)
    //        }
    //    }
    //
    //    // Request user to grant permissions for the app to use notifications
    //    func registerForPushNotifications() {
    //        UNUserNotificationCenter.current().delegate = self
    //        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
    //            (granted, error) in
    //            print("Permission granted: \(granted)")
    //            // 1. Check if permission granted
    //            guard granted else { return }
    //            // 2. Attempt registration for remote notifications on the main thread
    //            DispatchQueue.main.async {
    //                UIApplication.shared.registerForRemoteNotifications()
    //            }
    //        }
    //    }
    
    // Other app delegate methods...
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                
                guard granted else { return }
                self.getNotificationSettings()
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                print("Notification settings: \(settings)")
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //****************************************************
    // MARK: MBProgressHUD Methods
    //****************************************************
    
    func showHUD(_ hudTitle : String, onView: UIView) {
        
        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: onView, animated: true)
        //hud.mode = MBProgressHUDMode.AnnularDeterminate
        hud.bezelView.color = colorClear
        hud.contentColor = UIColor.white
        
        //hud.activityIndicatorColor = colorWhite
        
        hud.label.textColor = colorWhite
        hud.bezelView.color = UIColor.lightGray
        hud.label.font = UIFont(name: AppFontBook, size: 17)
        
        if Util.isValidString(hudTitle) {
            hud.label.text = hudTitle
        }
        else {
            hud.label.text = "Loading..."
        }
    }
    
    func hideHUD(_ fromView: UIView) {
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    //****************************************************
    // MARK: - Network Reachability Methods
    //****************************************************
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        
        let reachability = hostName == nil ? Reachability() : Reachability(hostname: hostName!)
        self.reachability = reachability
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.reachabilityChanged(_:)), name: Notification.Name.reachabilityChanged, object: reachability)
    }
    
    func startNotifier() {
        do {
            try reachability?.startNotifier()
        }
        catch {
            return
        }
    }
    
    func stopNotifier() {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: nil)
        reachability = nil
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        isNetworkAvailable = reachability.isReachable
        
        if reachability.isReachable {
            
            if UserDefaults.standard.bool(forKey: Key_UD_IsUserLoggedIn) && isNetworkAvailable && !isNetworkBecomeAvailableFirstTime {
                
                isNetworkBecomeAvailableFirstTime = true
                
                //** Post notification for |NetworkBecomeAvailableFirstTime|
                NotificationCenter.default.post(name: Notification.Name(rawValue: notificationNetworkBecomeAvailableFirstTime), object: self, userInfo:nil)
            }
        }
        else {
            if isNetworkBecomeAvailableFirstTime == false {
                NotificationCenter.default.post(name: Notification.Name(rawValue: notificationNetworkBecomeAvailableFirstTime), object: self, userInfo:nil)
            }
        }
    }
    
}
