//
//  UpdateNorificationVC.swift
//  SwipeStudent
//
//  Created by mac on 07/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class UpdateNorificationVC: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGrade: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    var strPersonId = ""
    var strName = ""
    var strGrade = ""
    
    var strPers = ""
    var strId = ""
    
    var arrIndexs = (0...6).map({ $0 })
    
    var arrNotificationads = NSArray()
    
    var arrMain  : NSDictionary = [:]
    
    var arrTemp  : NSDictionary = [:]
    
    var arrNotiFication = ["School Arrival", "School Tardy", "School Absence", "Consequence Issued", "Class Cuts", "Fines", "All Alerts"]
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblName.text = strName
        lblGrade.text = strGrade
        
        strPersonId = UserDefaults.standard.string(forKey: "PersonId")!
        
        let viewBg = UIView()
                viewBg.backgroundColor = UIColor.white
                
                tblNotification.backgroundView = viewBg
        
        //NotificationSubscribeAPI()
        
        NotificationListAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Method
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFinish_Action(_ sender: Any) {
        PeerAccountLinkAPI()
    }
    
    // MARK: - API Method
    
    
    func NotificationListAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        Networking.performApiCall(Networking.Router.NotificationList(), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    print(jsonObj)
                    
                    let data = jsonObj["data"]
                    print(data)
                    if (data != nil) {
                        self.arrMain = data.dictionary! as NSDictionary
                        print(self.arrMain)
                        let kjh = self.arrMain.allValues
                        self.arrNotificationads = kjh as NSArray
                        print(self.arrNotificationads)
                        self.arrNotiFication.removeAll()
                        
                        for value in self.arrNotificationads {
                            
                            let ljh: String = String(describing: value)
                            
                            print(ljh)
                            
                            self.arrNotiFication.append(ljh)
                        }
                        
                        
//                        self.dictMain["Relationship"] = "self" as AnyObject
//                        self.dictMain["StudentId"] = self.strId as AnyObject
//                        self.dictMain["NotificationOptions"] = self.arrNotiFication as AnyObject
//                        print(self.dictMain)
//
//                        self.arrStudentPeer.add(self.dictMain)
//                        self.dictSignUp["StudentPeerAccounts"] = self.dictMain as AnyObject
//                        print(self.dictSignUp)
                        
                        self.NotificationSubscribeAPI()
                        
                        self.tblNotification.reloadData()
                    }
                }
            }
        }
    }
    
    func NotificationSubscribeAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        let postParams: [String: AnyObject] =
            [
                "RecipientId"          : strPersonId as AnyObject,
            ]
        
        print("Post Parameter is:\(postParams)")
        
        Networking.performApiCall(Networking.Router.NotificationSubscribe(postParams), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    print(jsonObj)
                    
                    let data = jsonObj["data"][0]
                    print(data)
                    if (data != nil) {
                        self.strId = data["StudentId"].stringValue
                        self.strPers = data["StudentId"].stringValue
                        //self.strStudentId = data["StudentNumber"].stringValue
                        //self.strStudentIntId = data["StudentId"].stringValue
                        //self.strGranted = data["Granted"].stringValue
                        
                        let valuedata = data["NotificationOptions"].dictionaryValue
                        print(valuedata)
                        
                        let jjjj = valuedata as NSDictionary
                        print(jjjj)
                        
                        let kjh = valuedata.values
                        for lkj in valuedata.values {
                            print(lkj)
                        }
                        
                        for lkjd in valuedata.keys {
                            print(lkjd)
                        }
                        print(kjh)
                    }
                }
            }
        }
    }
    
    
    func PeerAccountLinkAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        let strCell = UserDefaults.standard.string(forKey: "PhoneNumber")
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        let postParams: [String: AnyObject] =
            [
                "PeerPersonId"          : strPersonId as AnyObject,
                "Relationship"          : "Self" as AnyObject,
                "StudentId"             : strId as AnyObject,
                "CellPhone"             : strCell as AnyObject,
                "NotificationOptions"   : self.arrNotiFication as AnyObject
        ]
        
        print("Post Parameter is:\(postParams)")
        
        Networking.performApiCall(Networking.Router.PeerAccountLink(postParams), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    print(jsonObj)
                    
                    if (jsonObj.arrayObject != nil) {
                        
                        let refreshAlert = UIAlertController(title: Key_Alert, message: "Update Succesfully", preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                            print("Handle Cancel Logic here")
                            
                            for loginVC in (self.navigationController?.viewControllers)! {
                             if loginVC.isKind(of: SettingVC.self) {
                             self.navigationController?.popToViewController(loginVC, animated: true)
                             return
                             }
                             }
                            
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                        
                    }
                }
            }
        }
    }
    
}


//****************************************************
// MARK: - UITableViewDataSource and Delegate
//****************************************************

extension UpdateNorificationVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotificationads.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tblNotification.dequeueReusableCell(withIdentifier: "SelectNotiCell") as? SelectNotiCell
        
        if cell == nil {
            cell = SelectNotiCell(style: UITableViewCellStyle.default, reuseIdentifier: "SelectNotiCell")
        }
        cell?.lblName.text =  "\(arrNotificationads[indexPath.row])"
        
        if arrIndexs.index(of: indexPath.row) != nil {
            // selected condition
            cell?.imgSelect.image = #imageLiteral(resourceName: "checked")
        } else {
            // unselected
            cell?.imgSelect.image = #imageLiteral(resourceName: "unchecked")
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrIndexs.index(of: indexPath.row) != nil {
            arrIndexs.removeObject(object: indexPath.row)
            arrNotiFication.removeObject(object: "\(arrNotificationads[indexPath.row])")
            print(arrNotiFication)
        } else {
            
            arrIndexs.append(indexPath.row)
            arrNotiFication.append("\(arrNotificationads[indexPath.row])")
            print(arrNotiFication)
            
        }
        tblNotification.reloadData()
        
        /* if (selectedIndexPaths.contains(indexPath as NSIndexPath))
         {
         selectedIndexPaths.removeObject(object: indexPath as NSIndexPath)
         print("enterrrrrrr")
         let str = arrNotificationads[indexPath.row]
         arrNotiFication.append("\(arrNotificationads[indexPath.row])")
         self.tblNotification.reloadData()
         print(self.arrNotiFication.count)
         
         print(arrNotiFication)
         }
         else
         {
         selectedIndexPaths.append(indexPath as NSIndexPath)
         print("outerrrrr")
         let str = arrNotificationads[indexPath.row]
         arrNotiFication.removeObject(object: "\(arrNotificationads[indexPath.row])")
         tblNotification.reloadData()
         print(arrNotiFication.count)
         print(arrNotiFication)
         }*/
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        print(arrNotiFication[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    /* //        if (selectedIndexPaths.contains(indexPath as NSIndexPath))
     //        {
     //            selectedIndexPaths.removeObject(object: indexPath as NSIndexPath)
     //
     //            var lkj = self.arrNotiFication
     //
     //            contents = [lkj.remove(at: indexPath.row)]
     //
     //            print(contents)
     //
     //        }
     //        else
     //        {
     //            selectedIndexPaths.append(indexPath as NSIndexPath)
     //
     //            //var lkj : [String] = self.arrNotiFication
     //            contents.append(self.arrNotiFication[indexPath.row])
     //           // contents = lkj.append(self.arrNotiFication[indexPath.row])
     //            print(contents)
     //        }
     ////
     ////        let cell = tableView.cellForRow(at: indexPath as IndexPath)!
     ////        populateCell(cell: cell, indexPath: indexPath as NSIndexPath )*/
}


