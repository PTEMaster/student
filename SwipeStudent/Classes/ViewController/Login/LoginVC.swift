//
//  LoginVC.swift
//  SwipeParent
//
//  Created by mac on 18/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import LocalAuthentication
import MBProgressHUD

class LoginVC: UIViewController {

    // MARK: - Properties
      let context : LAContext = LAContext()
      var authError: NSError?
      @IBOutlet weak var btnTitle: UIButton!
      @IBOutlet weak var btnAuthnticateImage: UIButton!
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            error: &authError) {
            if #available(iOS 11.0, *) {
                switch context.biometryType {
                case .faceID:
                    btnTitle.setTitle("Authenticate using your face", for: .normal)
                    btnAuthnticateImage.setImage(UIImage(named: "faceId"), for: .normal)
                case .touchID:
                    btnTitle.setTitle("Authenticate using your Finger", for: .normal)
                    btnAuthnticateImage.setImage(UIImage(named: "TouchId"), for: .normal)
                case .none:
                    btnTitle.isHidden = true
                    btnAuthnticateImage.isHidden = true
                default:
                    break
                }
            }
        }
         self.setupLogupCondition()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLogupCondition() {
           if UserDefaults.standard.isLoggedIn() == true, UserDefaults.standard.isTouchId() == true {
               self.authenticateUserTouchID()
           } else {
            print(UserDefaults.standard.isLoggedIn() ,UserDefaults.standard.isTouchId())
        }
       }
    
    
    @IBAction func actionAuthenticateLogin(_ sender: Any) {
        self.authenticateUserTouchID()
    }
    
    func authenticateUserTouchID() {
     
        // Declare a NSError variable.
        let myLocalizedReasonString = "Authentication is needed to access your Home."
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
          
            context.evaluatePolicy( .deviceOwnerAuthenticationWithBiometrics , localizedReason: myLocalizedReasonString) { success, evaluateError in
                // IF TOUCH ID AUTHENTICATION IS SUCCESSFUL, NAVIGATE TO NEXT VIEW CONTROLLER
                if success {
                    DispatchQueue.main.async{
                        
                        if let strOut = UserDefaults.standard.string(forKey: "BioDate") {
                              let df = DateFormatter()
                              df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                             let currentDate =  df.string(from: Date())
                          
                          
                              let dateString1 = strOut
                              let dateString2 = currentDate

                              let Dateformatter = DateFormatter()
                              Dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

                              let date1 = Dateformatter.date(from: dateString1)
                              let date2 = Dateformatter.date(from: dateString2)

                              let distanceBetweenDates: TimeInterval? = date2?.timeIntervalSince(date1!)
                              let secondsInAnHour: Double = 3600
                              let secondsInDays: Double = 86400
                              let secondsInWeek: Double = 604800

                              let hoursBetweenDates = Int((distanceBetweenDates! / secondsInAnHour))
                              let daysBetweenDates = Int((distanceBetweenDates! / secondsInDays))
                              let weekBetweenDates = Int((distanceBetweenDates! / secondsInWeek))

                              print(weekBetweenDates,"weeks")//0 weeks
                              print(daysBetweenDates,"days")//5 days
                              print(hoursBetweenDates,"hours")//120 hours
                          if hoursBetweenDates <= 189{
                              print("Authentication success by the system")
                              if UserDefaults.standard.isLoggedIn() {
                              //  self.performSegue(withIdentifier: "gotoHome", sender: self)
                                   UserDefaults.standard.setTouchid(value: true)
                                  let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey") ?? "1"
                                
                                let userName = UserDefaults.standard.string(forKey: "UserName")
                                // 39455@swipek12.com
                                if userName == "kaa004@swipek12.com"{
                                    let vc  = self.storyboard?.instantiateViewController(withIdentifier: "StudentHomeVC") as! StudentHomeVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                }else{
                                    if Int(LoginSurvey)! > 0{
                                        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "FirstStepVC") as! FirstStepVC
                                        self.navigationController?.pushViewController(nextControll, animated: true)
                                    }else{
                                        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "StudentHomeVC") as! StudentHomeVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                                
                                /*  if Int(LoginSurvey)! > 0{
                                      let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "FirstStepVC") as! FirstStepVC
                                      self.navigationController?.pushViewController(nextControll, animated: true)
                                  }else{
                                    let vc  = self.storyboard?.instantiateViewController(withIdentifier: "StudentHomeVC") as! StudentHomeVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                  }*/
                              } else {
                                Util.showAlertWithMessage(ConstantsMessage.valid.empty.kmanually, title: AppName)
                                //  self.performAlertViewController(title: AppName, message: ConstantsMessage.valid.empty.kmanually)
                              }
                              
                          }else{
                              UserDefaults.standard.logoutData()
                              UserDefaults.standard.logoutIsLogin()
                              UserDefaults.standard.logoutTouchId()
                              UserDefaults.standard.removeObject(forKey: "Permissions")
                              UserDefaults.standard.removeObject(forKey: "LoginSurvey")
                              UserDefaults.standard.removeObject(forKey: "UserName")
                              UserDefaults.standard.removeObject(forKey: "BioDate")
                            Util.showAlertWithMessage(ConstantsMessage.valid.empty.kmanually, title: AppName)
                          }
                      }else{
                          UserDefaults.standard.logoutData()
                          UserDefaults.standard.logoutIsLogin()
                          UserDefaults.standard.logoutTouchId()
                          UserDefaults.standard.removeObject(forKey: "Permissions")
                          UserDefaults.standard.removeObject(forKey: "LoginSurvey")
                          UserDefaults.standard.removeObject(forKey: "UserName")
                          UserDefaults.standard.removeObject(forKey: "BioDate")
                        Util.showAlertWithMessage(ConstantsMessage.valid.empty.kmanually, title: AppName)
                         
                      }
                        
                        
                        
                      /*  print("Authentication success by the system")
                        if UserDefaults.standard.isLoggedIn() {
                
                             UserDefaults.standard.setTouchid(value: true)
                            if  let LoginSurvey = Int(UserDefaults.standard.string(forKey: "LoginSurvey") ?? "1") {
                                if LoginSurvey > 0{
                                    let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "FirstStepVC") as! FirstStepVC
                                    self.navigationController?.pushViewController(nextControll, animated: true)
                                }else{
                                    let vc  = self.storyboard?.instantiateViewController(withIdentifier: "StudentHomeVC") as! StudentHomeVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        } else {
                            
                            let alertController = UIAlertController(title: AppName, message: ConstantsMessage.valid.empty.kmanually, preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (actionOk) in
                                let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "PasswordLoginViewController") as! PasswordLoginViewController
                                nextControll.isLogin = true
                                self.navigationController?.pushViewController(nextControll, animated: true)
                            }))
                            self.present(alertController, animated: true, completion: nil)
                        }*/
                        
                    }
                }
                else // IF TOUCH ID AUTHENTICATION IS FAILED, PRINT ERROR MSG
                {
                    guard let error = self.authError else {
                        return
                    }
               
                    let message = self.showErrorMessageForLAErrorCode(errorCode: error.code)
                        print(message)
                }
            }
        }
    }
    
    func showErrorMessageForLAErrorCode( errorCode:Int ) -> String{
          
          var message = ""
          
          switch errorCode {
              
          case LAError.appCancel.rawValue:
              message = "Authentication was cancelled by application"
              
          case LAError.authenticationFailed.rawValue:
              message = "The user failed to provide valid credentials"
              
          case LAError.invalidContext.rawValue:
              message = "The context is invalid"
              
          case LAError.passcodeNotSet.rawValue:
              message = "Passcode is not set on the device"
              
          case LAError.systemCancel.rawValue:
              message = "Authentication was cancelled by the system"
    
          case LAError.userCancel.rawValue:
              message = "The user did cancel"
              
          case LAError.userFallback.rawValue:
              message = "The user chose to use the fallback"
              
          default:
              message = "Did not find error code on LAError object"
              
          }
          
          return message
          
      }
    func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)

        layer.render(in: UIGraphicsGetCurrentContext()!)

        let outputImage = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return outputImage!
    }
    // MARK: - Action Method
    
    @IBAction func btnSignUp_Action(_ sender: Any) {
        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "SearchStdSclAddVC") as! SearchStdSclAddVC
        self.navigationController?.pushViewController(nextControll, animated: true)
    }
    
    @IBAction func btnPinCodeLogin_Action(_ sender: Any) {
        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "PasswordLoginViewController") as! PasswordLoginViewController
        nextControll.isLogin = true
        self.navigationController?.pushViewController(nextControll, animated: true)
    }
}


enum BiometricType {
    case none
    case touch
    case face
}
