//
//  StudentHomeVC.swift
//  SwipeStudent
//
//  Created by mac on 02/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire

class StudentHomeVC: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var imgBarCode: UIImageView!
    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var lblStudent: UILabel!
    @IBOutlet weak var lblHRnumber: UILabel!
    @IBOutlet weak var lblBusNumber: UILabel!
    @IBOutlet weak var btnGrade: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewSideMenu: UIView!
    @IBOutlet weak var constSideMenuLeading: NSLayoutConstraint!
    @IBOutlet weak var constSideMenuWidth: NSLayoutConstraint!
    @IBOutlet weak var ImgQRCode: UIImageView!
    @IBOutlet weak var btnHallPass: UIButton!
    var dict = [String: Any]()
    var arrStudentLogin : StudentHome!
    var strPersonId = ""
    var strStudentId = ""
    var strSchoolId = ""
    var strGranted = ""
    var strStudentIntId = ""
    //var strPersonId = ""
    var    strimg = ""
    
    var lastTime = ""
    let notificationCenter = NotificationCenter.default
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnHallPass.isHidden = true
        strPersonId = UserDefaults.standard.string(forKey: "PersonId")!
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        /*let strNumber    = String(arc4random_uniform(9999))
         let strNumberOne = String(arc4random_uniform(9999))
         let strDate      = Util.getStringFromDate(NSDate() as Date, sourceFormat: "yyyy-MM-dd HH:mm:ss ZZZ", destinationFormat: "yyyyMMddHHmmss")
         
         let strBarCode = "\(strNumber)\(strDate)\(strNumberOne)"
         let generator  = QRCodeGenerator()
         imgBarCode.image = generator.createImage(value: strBarCode,size: CGSize(width: 60, height: 60))*/
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.btnHallPass.isHidden = true
        //   self.postRequest()
        NotificationSubscribeAPI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    @objc func appMovedToBackground() {
        // do whatever event you want
        self.postRequest(false)
        
    }
    // MARK: - QR Code
    
    func generateBarcode(from string: String , nameBarCode:String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: nameBarCode) {
            filter.setValue(data, forKey: "inputMessage")
            //filter.setValue("H", forKey: "inputCorrectionLevel")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    func fromString(string : String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let output = filter?.outputImage?.transformed(by: transform)
        return UIImage(ciImage: output!)
    }
    
    // MARK: - Action Method
    
    @IBAction func btnRefresh_Action(_ sender: Any) {
        self.postRequest(false)
    }
    @IBAction func btnSideMenuDissmiss_Action(_ sender: Any) {
        viewSideMenu.isHidden = true
        constSideMenuWidth.constant   = -ScreenSize.width
        constSideMenuLeading.constant = -ScreenSize.width
    }
    
    @IBAction func btnSetting_Action(_ sender: Any) {
        let nextcontroll = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        nextcontroll.strName = lblStudent.text!
        nextcontroll.strGrade = (btnGrade.titleLabel?.text)!
        nextcontroll.strPerson = strPersonId
        self.navigationController?.pushViewController(nextcontroll, animated: true)
    }
    
    @IBAction func btnLogout_Action(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: "Are you sure do you want to logout?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (actionlogout) in
            UserDefaults.standard.set("false", forKey: "Loggedin")
            UserDefaults.standard.logoutIsLogin()
            UserDefaults.standard.logoutTouchId()
            UserDefaults.standard.logoutData()
            UserDefaults.standard.removeObject(forKey: "Permissions")
            UserDefaults.standard.removeObject(forKey: "LoginSurvey")
            UserDefaults.standard.removeObject(forKey: "BioDate")
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.setViewControllers([vc], animated: true)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func btnStudentDetail_Action(_ sender: Any) {
        
        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "StudentDetailsVC") as! StudentDetailsVC
        nextControll.strGranted  = strGranted
        nextControll.strSchoolId = strSchoolId
        nextControll.strStudentNumber = strStudentId
        nextControll.strIntStdId = strStudentIntId
        
        self.navigationController?.pushViewController(nextControll, animated: true)
    }
    
    @IBAction func btnMenu_Action(_ sender: Any) {
        viewSideMenu.isHidden = false
        constSideMenuWidth.constant   = ScreenSize.width
        constSideMenuLeading.constant = 0
    }
    
    // MARK: - API Method
    
    func NotificationSubscribeAPI() {
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        appDelegate.showHUD("Loading", onView: self.view)
        let postParams: [String: AnyObject] = ["RecipientId": strPersonId as AnyObject]
        print("Post Parameter is:\(postParams)")
        Networking.performApiCall(Networking.Router.NotificationSubscribe(postParams), callerObj: self, showHud:true) { (response) -> () in
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    //print(jsonObj)
                    let data = jsonObj["data"][0]
                    //   print(data)
                    if (data != nil) {
                        if data.count > 0{
                            self.strSchoolId = data["SchoolId"].stringValue
                            self.strStudentId = data["StudentNumber"].stringValue
                            self.strStudentIntId = data["StudentId"].stringValue
                            self.strGranted = data["Granted"].stringValue
                            self.postRequest(false)
                            self.StudentProfileAPI()
                        }else{
                            let uiAlert = UIAlertController(title: "Error", message: "No Data Found" , preferredStyle:UIAlertControllerStyle.alert)
                            appDelegate.window?.rootViewController!.present(uiAlert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    func StudentProfileAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        appDelegate.showHUD("Loading", onView: self.view)
        Networking.performApiCall(Networking.Router.StudentProfile(strSchoolId, strStudentId), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    let jsonObj = JSON(result)
                    print(jsonObj)
                    let data = jsonObj["data"]
                    print(data)
                    if (data != nil) {
                        let strimg = "https://\(data["ImageHost"].stringValue)\(data["ImageUrl"].stringValue)"
                        self.strimg = strimg
                        if let url = URL(string: strimg) {
                            self.imgProfile.kf.indicatorType = .activity
                            self.imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgDefault"), options: nil, progressBlock: nil, completionHandler: nil)
                        }

                        let strName = "\(data["FirstName"].stringValue) \(data["MiddleName"].stringValue) \(data["LastName"].stringValue)"
                        self.lblStudent.text = strName
                        self.lblBusNumber.text = "Bus : \(data["Bus"].stringValue)"
                        self.lblHRnumber.text  = "HR : \(data["Homeroom"].stringValue)"
                        self.btnGrade.setTitle("Grade : \(data["Grade"].stringValue)", for: .normal)
                        
                        //self.strPersonId = "\(String(describing: data["PersonId"].int))"
                        
                        let strBarCode = "\(data["StudentNumber"].stringValue)"
                        print(strBarCode)
                        self.ImgQRCode.image = self.generateBarcode(from: strBarCode, nameBarCode: "CIQRCodeGenerator")
                        self.imgBarCode.image = self.generateBarcode(from: strBarCode, nameBarCode: "CICode128BarcodeGenerator")
                        
                        
                        //                        let generator  = QRCodeGenerator()
                        //
                        //                        self.imgBarCode.image = generator.createImage(value: strBarCode,size: CGSize(width: 60, height: 60))
                        self.SearchSchoolAPI()
                    }
                }
            }
        }
    }
    
    
    func SearchSchoolAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        let postParams: [String: AnyObject] =
        [
            "criteria"          : strSchoolId as AnyObject
        ]
        
        print("Post Parameter is:\(postParams)")
        
        Networking.performApiCall(Networking.Router.SearchSchool(postParams), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    print(jsonObj)
                    
                    if (jsonObj.arrayObject != nil) {
                        
                        self.lblSchoolName.text = jsonObj[0]["SchoolName"].stringValue
                    }
                }
            }
        }
    }
}

extension StudentHomeVC{
    func postRequest(_ isCalled : Bool) {
        let parameters: [String: Any] = [
            "StudentNumber" : strStudentId,
            "SchoolId": strSchoolId,
            "StudentId": strStudentIntId
        ]
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        print("http://beta-api.swipek12.com/dataservice/\(strSchoolId)/StudentLocation")
        print(parameters)
        Alamofire.request("http://beta-api.swipek12.com/dataservice/\(strSchoolId)/StudentLocation", method:.post, parameters: parameters,encoding: JSONEncoding.default) .responseJSON { (response) in
            appDelegate.hideHUD(self.view)
            if let result = response.result.value {
                
                let jsonObj = JSON(result)
                self.dict = jsonObj.rawValue as! [String : Any]
                print("Student location data if:- ",self.dict)
                //self.btnHallPass.isHidden = false
                if (self.dict["active"] as? Int) != nil {
                    let status = self.dict["active"] as? Int
                    if status == 0 {
                        self.btnHallPass.isHidden = true
                        TimerModal.sharedTimer.stopTimer()
                    }else {
                        self.btnHallPass.isHidden = false
                        self.lastTime = self.dict["leaveTime"] as? String ?? ""
                        // TimerModal.sharedTimer.startTimer(withInterval: 1) {
                        //                            self.diffrenceTime(time2Str: self.lastTime)
                        //                        }
                        if isCalled{
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
                            vc.dict = self.dict
                            vc.strimg = self.strimg
                            vc.modalPresentationStyle = .overCurrentContext
                            vc.modalTransitionStyle = .crossDissolve
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                
            }else{
                print("Student location data else hide button:- ", response.value)
                self.btnHallPass.isHidden = true
                TimerModal.sharedTimer.stopTimer()
            }
        }
    }
    @IBAction func actionHallPass(_ sender: Any) {
        self.postRequest(true)
    }
}
