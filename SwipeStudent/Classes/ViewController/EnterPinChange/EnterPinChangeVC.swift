//
//  EnterPinChangeVC.swift
//  SwipeStudent
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class EnterPinChangeVC: UIViewController, UITextFieldDelegate {

    // MARK: - Properties
    
    @IBOutlet weak var txfEnterOldPin: UITextField!
    @IBOutlet weak var txfEnterNewPin: UITextField!
    @IBOutlet weak var txfEnterConfirm: UITextField!
    
    
    var strId = ""
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        strId = UserDefaults.standard.string(forKey: "UserName")!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private Method
    
    func validation() -> Bool {
        if !Util.isValidString(txfEnterOldPin.text!) {
            Util.showAlertWithMessage("Please enter old password", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfEnterNewPin.text!) {
            Util.showAlertWithMessage("Please enter new passwrod", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfEnterConfirm.text!) {
            Util.showAlertWithMessage("Please enter confirm password", title: Key_Alert)
            return false
        } else if ((txfEnterNewPin.text) != (txfEnterConfirm.text)) {
            Util.showAlertWithMessage("Password and confirm password should be same", title: Key_Alert)
            return false
        }
        
        return true
    }

    // MARK: - Action Method
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPasswordDone_Action(_ sender: Any) {
        if validation() {
            EnterPinChangeAPI()
        }
    }
    
    /*func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 4 // Bool
    }*/
    
    // MARK: - API Method

    func EnterPinChangeAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        let postParams: [String: AnyObject] =
            [
                "username" : strId as AnyObject,
                "oldPassword" : txfEnterOldPin.text as AnyObject,
                "newPassword" : txfEnterConfirm.text as AnyObject
        ]
        
        print("Post Parameter is:\(postParams)")
        
        Networking.performApiCall(Networking.Router.PasswordChange(postParams), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    let msg = jsonObj["responseText"].stringValue
                    print(jsonObj)
                    Util.showAlertWithMessage(msg, title: Key_Alert)
                        self.txfEnterNewPin.text = ""
                        self.txfEnterOldPin.text = ""
                        self.txfEnterConfirm.text = ""

                }
            }
        }
    }
}
