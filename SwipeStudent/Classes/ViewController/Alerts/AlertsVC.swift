//
//  AlertsVC.swift
//  SwipeStudent
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class AlertsVC: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var tblAttendenceList: UITableView!
    var arrPeriodScanc = NSMutableArray()
    
    var strStudentNumber = ""
    var strSchId = ""
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let viewBg = UIView()
        viewBg.backgroundColor = UIColor.white
        
        tblAttendenceList.backgroundView = viewBg
        
        AlertsAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Action Method
    
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - API Method

    func AlertsAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        Networking.performApiCall(Networking.Router.Alerts(strSchId, strStudentNumber), callerObj: self, showHud:true) { (response) -> () in
            appDelegate.hideHUD(self.view)
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    let jsonObj = JSON(result)
                    print(jsonObj)
                    
                    let data = jsonObj["data"]
                    print(data)
                    if (data != nil) {
                        
                        self.arrPeriodScanc.removeAllObjects()

                        let arrSites = NSMutableArray.init(array:Alert.getPeriodScans(response.result.value! as AnyObject))

                        if (arrSites.count > 0) {
                            self.arrPeriodScanc.addObjects(from: arrSites as [AnyObject])
                        }

                        self.tblAttendenceList.reloadData()
                        print(self.arrPeriodScanc)
                    }
                }
            }
        }
    }
}

extension AlertsVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //if isPeriodReload == true {
        //   return self.arrPeriodScanc.count
        //} else {
        print("oooooooooooooooooo\(self.arrPeriodScanc.count)")
        return self.arrPeriodScanc.count
        //}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        let cell1 = tblAttendenceList.dequeueReusableCell(withIdentifier: "StudentCell") as! StudentCell
        let obj = self.arrPeriodScanc.object(at: indexPath.row) as! Alert
        cell1.lblSPeriod.text = obj.DisplayText
        cell1.lblSRoom.text   = obj.OutcomeType
        cell1.lblSClass.text  = obj.LongStartDate
        cell = cell1
        
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        //if tableView == tblList{
    //        let cell1 = tblAttendenceList.dequeueReusableCell(withIdentifier: "StudentCell") as! StudentCell
    //
    //        //cell1.lblName.backgroundColor    = UIColor.brown
    //        //cell1.lblName.backgroundColor    = UIColor(rgbValue: 0x0349bd)
    //        //cell1.lblSClass.backgroundColor  = UIColorFromRGB(rgbValue: 0x0349bd)
    //        //cell1.lblSPeriod.backgroundColor = UIColorFromRGB(rgbValue: 0x0349bd)
    //
    //        return cell1
    //
    //        // }
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let additionalSeparatorThickness = CGFloat(0.6)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.darkGray
        cell.addSubview(additionalSeparator)
    }
}

