//
//  SettingVC.swift
//  SwipeStudent
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {

    // MARK: - Properties
    
    var strName = ""
    var strGrade = ""
    var strPerson = ""
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Action Method
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnStudentNotification_Action(_ sender: Any) {
        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "UpdateNorificationVC") as! UpdateNorificationVC
        nextControll.strName = strName
        nextControll.strGrade = strGrade
        nextControll.strPers = strPerson
        self.navigationController?.pushViewController(nextControll, animated: true)
    }
    
    @IBAction func btnChangePassword_Action(_ sender: Any) {
        
        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "EnterPinChangeVC") as! EnterPinChangeVC
        self.navigationController?.pushViewController(nextControll, animated: true)
    }
    
    // MARK: - API Method
    

}
