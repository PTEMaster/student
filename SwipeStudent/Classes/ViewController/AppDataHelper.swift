//
//  DataHelper.swift
//  SwipeK Teacher
//
//  Created by mac on 13/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation

extension UserDefaults {

    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    func setUserName(value:String) {
        set(value, forKey: UserDefaultsKeys.userName.rawValue)
    }
   
    func isLoggedIn()-> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    //MARK:- Check TouchId
    func setTouchid(value:Bool) {
        set(value, forKey: UserDefaultsKeys.isTouchId.rawValue)
    }
    func isTouchId()-> Bool {
        return bool(forKey: UserDefaultsKeys.isTouchId.rawValue)
    }
    
    //MARK: Save User Data
    func setUserData(value:[String:Any]){
        set(value, forKey: UserDefaultsKeys.loginData.rawValue)
        //synchronize()
    }

    //integer(forKey: UserDefaultsKeys.loginData.rawValue)
    //MARK: Retrieve User Data
    func getUserData() -> [String:Any] {
        return dictionary(forKey: UserDefaultsKeys.loginData.rawValue)!
    }
    func getUserName() -> String {
        return string(forKey:  UserDefaultsKeys.userName.rawValue) ?? ""
    }
    
    func logoutData() {
        return removeObject(forKey: UserDefaultsKeys.loginData.rawValue)
    }
    
    func logoutTouchId() {
       return removeObject(forKey: UserDefaultsKeys.isTouchId.rawValue)
    }
    
    func logoutIsLogin() {
        return removeObject(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
   
}

enum UserDefaultsKeys : String {
    case isLoggedIn
    case loginData
    case isTouchId
    case userName
}


  let AppName = "Swipe Student id"

struct ConstantsMessage {
    struct valid {
        struct empty {
            static let kuserName = "Please enter your username"
            static let kpassword = "Please enter you password"
            static let kmanually = "Please first time manually login"
            static let kAttendance = "Please select Attendance"
        }
    }
}
