//
//  SearchStdSclSecondVC.swift
//  SwipeStudent
//
//  Created by mac on 30/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class SearchStdSclSecondVC: UIViewController {
    
    @IBOutlet weak var txfSchoolId: UITextField!
    @IBOutlet weak var viewSchoolSearch: UIView!
    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var constSchoolView: NSLayoutConstraint!
    @IBOutlet weak var viewClear: UIView!
    @IBOutlet weak var constViewClear: NSLayoutConstraint!
    @IBOutlet weak var btnNext: UIButton!

    var dictSignUp : [String: AnyObject] = [:]
    var isSchoolId = false


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.btnNext.isHidden = true

    }
    
    
    
    // MARK: - API Methods
    func SearchSchoolAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        let postParams: [String: AnyObject] =
            [
                "criteria"          : txfSchoolId.text as AnyObject
         ]
        
        print("Post Parameter is:\(postParams)")
        
        Networking.performApiCall(Networking.Router.SearchSchool(postParams), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    print(jsonObj)
                    
                    if (jsonObj.arrayObject != nil) {
                        
                        self.isSchoolId = true
                        print("alsdjfjkasdhfkadhsfkjhaksldjfh")
                        
                        self.lblSchoolName.text = jsonObj[0]["SchoolName"].stringValue
                        
                        self.viewSchoolSearch.isHidden = false
                        
                        self.constSchoolView.constant = 60
                         self.btnNext.isHidden = false
                        
                    }
                
                }
            }
        }
    }
    

    @IBAction func btnGoSchoolIdSearch_Action(_ sender: Any) {
        self.view.endEditing(true)
        if !Util.isValidString(txfSchoolId.text!) {
            Util.showAlertWithMessage("Please enter school Id", title: Key_Alert)
        } else {
            SearchSchoolAPI()
        }
    }
    
    @IBAction func btnNext_Action(_ sender: Any) {
        
        if !Util.isValidString(txfSchoolId.text!) {
            Util.showAlertWithMessage("Please enter school Id", title: Key_Alert)
        } else {
            let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "SearchStdSclThirdVC") as! SearchStdSclThirdVC
            nextControll.dictSignUp = dictSignUp
            nextControll.strSclId = (txfSchoolId.text as AnyObject) as! String
            
            self.viewSchoolSearch.isHidden = true
            self.constSchoolView.constant = 0
            self.btnNext.isHidden = true
            self.txfSchoolId.text = ""
            self.navigationController?.pushViewController(nextControll, animated: true)
        }
    }

    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
}
