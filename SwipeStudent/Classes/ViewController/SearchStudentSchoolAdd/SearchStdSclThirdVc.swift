//
//  SearchStdSclThirdVC.swift
//  SwipeStudent
//
//  Created by mac on 30/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit


class SearchStdSclThirdVC: UIViewController {
    
    @IBOutlet weak var txfStudentId: UITextField!
    @IBOutlet weak var viewStudentSearch: UIView!
    @IBOutlet weak var lblStudentName: UILabel!
    @IBOutlet weak var lblGrade: UILabel!
    @IBOutlet weak var imgStudent: UIImageView!
    @IBOutlet weak var constStudentView: NSLayoutConstraint!
    @IBOutlet weak var viewClear: UIView!
    @IBOutlet weak var constViewClear: NSLayoutConstraint!
    @IBOutlet weak var btnNext: UIButton!

    
    var dictSignUp : [String: AnyObject] = [:]
    var strStudentid = ""
    var strName = ""
    var strGrad = ""
    var strId  = ""
    var strSclId = ""
    var isSchoolId = false


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // btnNext.isEnabled = false
        self.btnNext.isHidden = true

    }
    
    func SearchStudentAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        let postParams: [String: AnyObject] =
            [
                "criteria"  : txfStudentId.text as AnyObject
          ]
        
        print("Post Parameter is:\(postParams)")
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        Networking.performApiCall(Networking.Router.SearchStudent(strSclId, postParams), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    print(jsonObj)
                    
                    if (jsonObj.arrayObject != nil) {
                        let fName = jsonObj[0]["FirstName"].stringValue
                        let lName = jsonObj[0]["LastName"].stringValue
                        let kjh = jsonObj[0]["StudentId"].int!
                        self.strStudentid = "\(kjh)"
                        
                        self.lblStudentName.text = "\(fName) \(lName)"
                        
                        self.viewStudentSearch.isHidden = false
                        self.constStudentView.constant = 100
                        
                        self.lblGrade.text = "Grade : \(jsonObj[0]["Grade"].stringValue)"
                        
                        self.viewClear.isHidden = true
                        self.constViewClear.constant = 0
                       // self.btnNext.isEnabled = true
                        self.btnNext.isHidden = false

                    }
            
                }
            }
        }
    }
    
    @IBAction func btnStudentIdSearch_Action(_ sender: Any) {

        self.view.endEditing(true)
        if !Util.isValidString(txfStudentId.text!) {
            Util.showAlertWithMessage("Please enter student Id", title: Key_Alert)
        } else {
            SearchStudentAPI()
        }
    }

    @IBAction func btnNext_Action(_ sender: Any) {
       
        if !Util.isValidString(txfStudentId.text!) {
            Util.showAlertWithMessage("Please enter student Id", title: Key_Alert)
        } else {
            let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "SelectNotificationVC") as! SelectNotificationVC
            nextControll.dictSignUp = dictSignUp
            nextControll.strId = self.strStudentid
            nextControll.strName = self.lblStudentName.text!
            nextControll.strGrad = self.lblGrade.text!
            self.txfStudentId.text = ""
            self.constStudentView.constant = 0
            viewStudentSearch.isHidden = true
            lblGrade.text =  ""
            lblStudentName.text = ""
            self.navigationController?.pushViewController(nextControll, animated: true)
        }
    }
    
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func btnClear_Action(_ sender: Any) {
    
    }
}
