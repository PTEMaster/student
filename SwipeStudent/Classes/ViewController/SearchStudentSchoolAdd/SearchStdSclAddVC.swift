//
//  SearchStdSclAddVC.swift
//  SwipeParent
//
//  Created by mac on 21/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class SearchStdSclAddVC: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var txfPhoneNumber: PhoneFormattedTextField!
    @IBOutlet weak var txfEmailId: UITextField!
    @IBOutlet weak var txtfPassword: UITextField!
    @IBOutlet weak var txtfConfirmPassword: UITextField!

    var strRelation = ""
    var dictSignUp : [String: AnyObject] = [:]
    var isSchoolId = false
    var strStudentid = ""
 
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //txfPhoneNumber.becomeFirstResponder()
        
        txfPhoneNumber.textDidChangeBlock = { field in
            if let text = field?.text {
                print(text)
            } else {
                print("No text")
            }
            
        }
        
        defaultExample()
    
        //Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func defaultExample() {
        txfPhoneNumber.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "###-###-####")
    }
    
    // MARK: - Validation
    func isValidRegister() -> Bool {
        if !Util.isValidString(txfPhoneNumber.text!) {
            Util.showAlertWithMessage("Please enter phone number", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfEmailId.text!) {
            Util.showAlertWithMessage("Please enter email Id", title: Key_Alert)
            return false
        } else if !Util.isValidEmail(txfEmailId.text!) {
            Util.showAlertWithMessage("Please enter valid email address", title: Key_Alert)
            return false
        } else if !Util.isValidString(txtfPassword.text!) {
            Util.showAlertWithMessage("Please enter password", title: Key_Alert)
        } else if txtfPassword.text != txtfConfirmPassword.text {
          Util.showAlertWithMessage("Do not match the confirm password", title: Key_Alert)
        }

        return true
    }

    // MARK: - Action
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnNext_Action(_ sender: Any) {
        
        if isValidRegister() {

            let vaue = txfPhoneNumber.text?.replacingOccurrences(of: "-", with: "")
            
            let uuid = NSUUID().uuidString
            
            dictSignUp["FirstName"]     = "" as AnyObject
            dictSignUp["LastName"]      = "" as AnyObject
            dictSignUp["Street"]        = "" as AnyObject
            dictSignUp["City"]          = "" as AnyObject
            dictSignUp["State"]         = "" as AnyObject
            dictSignUp["PostalCode"]    = "" as AnyObject
            dictSignUp["Email"]         = txfEmailId.text as AnyObject
            dictSignUp["CellPhone"]     = vaue as AnyObject
            dictSignUp["IsMobile"]      = true as AnyObject
            dictSignUp["serial_number"] = uuid as AnyObject
            dictSignUp["password"]      = txtfConfirmPassword.text as AnyObject
            dictSignUp["Pin"]           = txtfConfirmPassword.text as AnyObject
            UserDefaults.standard.set(txfEmailId.text, forKey: "CellNumber") //setObject
            let secondStdVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchStdSclSecondVC") as! SearchStdSclSecondVC
            secondStdVC.dictSignUp = dictSignUp
            self.txfEmailId.text = ""
            self.txfPhoneNumber.text = ""
            self.navigationController?.pushViewController(secondStdVC, animated: true)

        }
    }
}
 
