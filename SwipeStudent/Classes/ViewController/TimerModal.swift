//
//  TimerModal.swift
//  SwipeStudent
//
//  Created by Himanshu Pal on 21/06/22.
//  Copyright © 2022 mac. All rights reserved.
//

import Foundation
class TimerModal: NSObject {
    static let sharedTimer: TimerModal = {
       let timer = TimerModal()
        return timer
    }()

    var internalTimer: Timer?
    var jobs = [() -> Void]()

    func startTimer(withInterval interval: Double, andJob job: @escaping () -> Void) {
        if internalTimer == nil {
            internalTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(doJob), userInfo: nil, repeats: true)
            jobs.append(job)
        }
       
    }

    func pauseTimer() {
        guard internalTimer != nil else {
           
            return
        }
        internalTimer?.invalidate()
        
    }

    func stopTimer() {
        guard internalTimer != nil else {
           
            return
        }
        jobs = [()->()]()
        internalTimer?.invalidate()
        internalTimer = nil
    }

    @objc func doJob() {
        guard jobs.count > 0 else { return }
        for job in jobs {
            job()
        }
    }

}

