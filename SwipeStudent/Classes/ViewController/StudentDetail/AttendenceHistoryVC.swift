//
//  AttendenceHistoryVC.swift
//  SwipeStudent
//
//  Created by mac on 05/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class AttendenceHistoryVC: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var tblAttendenceList: UITableView!
    @IBOutlet weak var lblNameStudent: UILabel!
    @IBOutlet weak var viewAbsenceTardy: UIView!
    @IBOutlet weak var btnDailyAtten: UIButton!
    @IBOutlet weak var btnPeriodScane: UIButton!
    @IBOutlet weak var btnAbsence: UIButton!
    @IBOutlet weak var btnTardy: UIButton!
    
    var arrPeriodScanc = NSMutableArray()
    var arrTardy = NSMutableArray()
    var arrAbsence = NSMutableArray()
    var arrAbsTar = NSMutableArray()
    var strSchId = ""
    var strStudentNumber = ""
    var istardy = true
    var isAbsenceReload = true
    var isTardyReload = false
    var isPeriodReload = false
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //StudentPeriodScance()
        let viewBg = UIView()
        viewBg.backgroundColor = UIColor.white
        
        tblAttendenceList.backgroundView = viewBg
         AttendenceDailyAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Method
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDailyAtten_Action(_ sender: Any) {
        isPeriodReload = false
        viewAbsenceTardy.isHidden = false
        btnPeriodScane.backgroundColor = UIColor.gray
        btnDailyAtten.backgroundColor = colorBlue
        AttendenceDailyAPI()
    }
    
    @IBAction func btnPeriodScanse_Action(_ sender: Any) {
        viewAbsenceTardy.isHidden = true
        isPeriodReload = true
        btnPeriodScane.backgroundColor = colorBlue
        btnDailyAtten.backgroundColor = UIColor.gray
        StudentPeriodScance()
    }
    
    @IBAction func btnAbsence_Action(_ sender: Any) {
        isAbsenceReload = true
        self.arrPeriodScanc.removeAllObjects()
        self.arrPeriodScanc = self.arrAbsence.mutableCopy() as! NSMutableArray
        btnTardy.setTitleColor(UIColor.brown, for: .normal)
        btnAbsence.setTitleColor(colorBlue, for: .normal)
        tblAttendenceList.reloadData()
    }
    
    @IBAction func btnTardy_Action(_ sender: Any) {
        isAbsenceReload = false
        self.arrPeriodScanc.removeAllObjects()
        self.arrPeriodScanc = self.arrTardy.mutableCopy() as! NSMutableArray
        btnTardy.setTitleColor(colorBlue, for: .normal)
        btnAbsence.setTitleColor(UIColor.brown, for: .normal)
        tblAttendenceList.reloadData()
    }
    
    // MARK: - API Method
    
    func StudentPeriodScance() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        Networking.performApiCall(Networking.Router.StudentPeriodScans(strSchId, strStudentNumber), callerObj: self, showHud:true) { (response) -> () in
            appDelegate.hideHUD(self.view)
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    let jsonObj = JSON(result)
                    print(jsonObj)
                    
                    let data = jsonObj["data"]
                    print(data)
                    if (data != nil) {
                        
                        self.arrPeriodScanc.removeAllObjects()
                        
                        let arrSites = NSMutableArray.init(array:PeriodScans.getPeriodScans(response.result.value! as AnyObject))
                        
                        if (arrSites.count > 0) {
                            self.arrPeriodScanc.addObjects(from: arrSites as [AnyObject])
                        }
                        
                        self.tblAttendenceList.reloadData()
                        print(self.arrPeriodScanc)
                    }
                }
            }
        }
    }
    
    func AttendenceDailyAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        Networking.performApiCall(Networking.Router.AttendanceDayRange(strSchId, strStudentNumber), callerObj: self, showHud:true) { (response) -> () in
            appDelegate.hideHUD(self.view)
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    let jsonObj = JSON(result)
                    print(jsonObj)
                    
                    let data = jsonObj[]
                    print(data)
                    if (data != nil) {
                        self.arrAbsTar.removeAllObjects()
                        
                        let arrSites = NSMutableArray.init(array:AttendenceDaily.getPeriodScans(response.result.value! as AnyObject))
                        
                        if (arrSites.count > 0) {
                            self.arrAbsTar.addObjects(from: arrSites as [AnyObject])
                        }
                        
                        //self.tblAttendenceList.reloadData()
                        print(self.arrAbsTar)
                        self.arrTardy.removeAllObjects()
                        self.arrAbsence.removeAllObjects()
                        
                        for arrD in self.arrAbsTar {
                            let val = arrD as! AttendenceDaily
                            
                            if let exc = val.Excuse {
                                print(exc as Any)
                                
                                if exc > 0 {
                                    print(exc)
                                    
                                    for arrExc in val.Excuses! {
                                        if let intId = arrExc["Id"].int {
                                             print(intId)
                                            
                                            if intId == exc {
                                                print("enterrrrrrr value")
                                                if let tardy = arrExc["TardyExcuse"].bool {
                                                    print(tardy)
                                                    if self.istardy == tardy {
                                                        self.arrTardy.add(arrD)
                                                    } else {
                                                        self.arrAbsence.add(arrD)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //print(self.arrTardy)
                        //print(self.arrAbsence)
                        
                        if self.isAbsenceReload == true {
                            self.arrPeriodScanc = self.arrAbsence.mutableCopy() as! NSMutableArray
                        } else {
                            self.arrPeriodScanc = self.arrTardy.mutableCopy() as! NSMutableArray
                        }
                        
                        self.tblAttendenceList.reloadData()
                    }
                }
            }
        }
    }
}

extension AttendenceHistoryVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //if isPeriodReload == true {
         //   return self.arrPeriodScanc.count
        //} else {
        print("oooooooooooooooooo\(self.arrPeriodScanc.count)")
        return self.arrPeriodScanc.count
        //}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        let cell1 = tblAttendenceList.dequeueReusableCell(withIdentifier: "StudentCell") as! StudentCell
        
        if isPeriodReload == true {
            let obj = self.arrPeriodScanc.object(at: indexPath.row) as! PeriodScans
            cell1.lblSPeriod.text = obj.Description
            cell1.lblSRoom.text   = obj.Name
            cell1.lblSClass.text  = obj.LongFormattedClassDate
            cell = cell1
        } else {
            
            let obj = self.arrPeriodScanc.object(at: indexPath.row) as! AttendenceDaily
             //  cell1.lblSPeriod.text = obj.de
            cell1.lblSRoom.text   = obj.status
            cell1.lblSClass.text  = "\(String(describing: obj.Date!)) \(String(describing: obj.EntryTime!))"
            
            if let exc = obj.Excuse {
                print(exc as Any)
                
                if exc > 0 {
                    print(exc)
                    
                    for arrExc in obj.Excuses! {
                        if let intId = arrExc["Id"].int {
                            print(intId)
                            
                            if intId == exc {
                                print("enterrrrrrr value")
                                if let tardy = arrExc["TardyExcuse"].bool {
                                    print(tardy)
//                                    if self.istardy == tardy {
//                                        self.arrTardy.add(arrD)
//                                    } else {
//                                        self.arrAbsence.add(arrD)
//                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            
            //cell1.lblSPeriod.text = "obj.Description"
            cell = cell1
        }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        //if tableView == tblList{
//        let cell1 = tblAttendenceList.dequeueReusableCell(withIdentifier: "StudentCell") as! StudentCell
//
//        //cell1.lblName.backgroundColor    = UIColor.brown
//        //cell1.lblName.backgroundColor    = UIColor(rgbValue: 0x0349bd)
//        //cell1.lblSClass.backgroundColor  = UIColorFromRGB(rgbValue: 0x0349bd)
//        //cell1.lblSPeriod.backgroundColor = UIColorFromRGB(rgbValue: 0x0349bd)
//
//        return cell1
//
//        // }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let additionalSeparatorThickness = CGFloat(0.6)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.darkGray
        cell.addSubview(additionalSeparator)
    }
}

