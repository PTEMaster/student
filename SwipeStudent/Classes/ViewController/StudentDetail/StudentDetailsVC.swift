//
//  StudentDetailsVC.swift
//  SwipeStudent
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class StudentDetailsVC: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewTableSelect: UIView!
    @IBOutlet weak var viewBottomBar: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblNameStudent: UILabel!
    @IBOutlet weak var lblStudentNumber: UILabel!
    @IBOutlet weak var btnGrade: UIButton!
    @IBOutlet weak var lblHR: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var constBottomHeight: NSLayoutConstraint!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var constTableListHeight: NSLayoutConstraint!
    
    var strGranted = ""
    var strStudentNumber = ""
    var strSchoolId = ""
    var strIntStdId = ""
    
    var arrSchedule  = NSMutableArray()
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //viewBottomBar.isHidden = true
        //constBottomHeight.constant = 54
        
        let viewBg = UIView()
              viewBg.backgroundColor = UIColor.white
              tblList.backgroundView = viewBg
        
        if strGranted == "false" {
            viewBottomBar.isHidden = true
            constBottomHeight.constant = 0
            viewMessage.isHidden = false
            viewTableSelect.isHidden = true
        } else {
            viewBottomBar.isHidden = false
            constBottomHeight.constant = 54
            viewTableSelect.isHidden = false
            viewMessage.isHidden = true
        }
        
        StudentProfileAPI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        constTableListHeight.constant = tblList.contentSize.height
        view.layoutIfNeeded()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Action Method
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnStudentDetail_Action(_ sender: Any) {
        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "AttendenceHistoryVC") as! AttendenceHistoryVC
        nextControll.strStudentNumber = strIntStdId
        nextControll.strSchId = strSchoolId
        self.navigationController?.pushViewController(nextControll, animated: true)
    }
    
    @IBAction func btnConsquence_Action(_ sender: Any) {
        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "ConsequenceVC") as! ConsequenceVC
        nextControll.strStudentNumber = strIntStdId
        nextControll.strSchId = strSchoolId
        self.navigationController?.pushViewController(nextControll, animated: true)
    }
    
    @IBAction func btnAlert_Action(_ sender: Any) {
        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "AlertsVC") as! AlertsVC
        nextControll.strStudentNumber = strIntStdId
        nextControll.strSchId = strSchoolId
        self.navigationController?.pushViewController(nextControll, animated: true)
    }
    
    // MARK: - API Method

    func StudentProfileAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        strStudentNumber = strStudentNumber.replacingOccurrences(of: " ", with: "")
        
        Networking.performApiCall(Networking.Router.StudentProfile(strSchoolId, strStudentNumber), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    let jsonObj = JSON(result)
                    print(jsonObj)
                    
                    let data = jsonObj["data"]
                    print(data)
                    if (data != nil) {
                        
                        self.ScheduelListAPI()
                        
                        let strimg = "https://\(data["ImageHost"].stringValue)\(data["ImageUrl"].stringValue)"
                        
                        if let url = URL(string: strimg) {
                            self.imgProfile.kf.indicatorType = .activity
                            self.imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgDefault"), options: nil, progressBlock: nil, completionHandler: nil)
                        }
                        
                        let strName = "\(data["FirstName"].stringValue) \(data["MiddleName"].stringValue) \(data["LastName"].stringValue)"
                        self.lblNameStudent.text = strName
                        self.lblStatus.text = data["Active"].stringValue
                        self.lblStudentNumber.text = "\(data["StudentNumber"].stringValue)"
                        //self.lblBusNumber.text = "Bus : \(data["Bus"].stringValue)"
                        self.lblHR.text  = "HR : \(data["Homeroom"].stringValue)"
                        self.btnGrade.setTitle("Grade : \(data["Grade"].stringValue)", for: .normal)
                        
                    }
                }
            }
        }
    }
    
    func ScheduelListAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        strStudentNumber = strStudentNumber.replacingOccurrences(of: " ", with: "")
        
        Networking.performApiCall(Networking.Router.Schedule(strSchoolId, strIntStdId), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    let jsonObj = JSON(result)
                    print(jsonObj)
                    
                    let data = jsonObj
                    print(data)
                    if (data != nil) {
            
                        let arrSites = NSMutableArray.init(array:StudentHome.getScheduel(response.result.value! as AnyObject))
                        
                        if (arrSites.count > 0) {
                            self.arrSchedule.addObjects(from: arrSites as [AnyObject])
                        }
                        
                        self.tblList.reloadData()
                        print(self.arrSchedule)
                        
                    }
                }
            }
        }
    }
}

extension StudentDetailsVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSchedule.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
            let cell1 = tblList.dequeueReusableCell(withIdentifier: "StudentCell") as! StudentCell
            let obj = self.arrSchedule.object(at: indexPath.row) as! StudentHome
            //SectionCode
            //let period = obj["PerName"] as? String;
            //let day = obj["DayName"] as? String;
            
            cell1.lblSPeriod.text = obj.PerName
            cell1.lblSRoom.text =  obj.RoomName
            cell1.lblSClass.text =  obj.ClassName
            cell = cell1
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //if tableView == tblList{
            let cell1 = tblList.dequeueReusableCell(withIdentifier: "StudentCellHeader") as! StudentCell

            return cell1
    }
    

    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let additionalSeparatorThickness = CGFloat(0.6)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.darkGray
        cell.addSubview(additionalSeparator)
        constTableListHeight.constant = tableView.contentSize.height
        view.layoutIfNeeded()
    }
}

