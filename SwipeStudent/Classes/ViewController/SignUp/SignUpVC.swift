//
//  SignUpVC.swift
//  SwipeParent
//
//  Created by mac on 18/06/18.
//  Copyright © 2018 mac. All rights reserved.
//
import UIKit

class SignUpVC: UIViewController, UIImagePickerControllerDelegate {

    // MARK: - Properties
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var txfFirstName: UITextField!
    @IBOutlet weak var txfLastName: UITextField!
    @IBOutlet weak var txfStreet: UITextField!
    @IBOutlet weak var txfCity: UITextField!
    @IBOutlet weak var txfState: UITextField!
    @IBOutlet weak var txfZip: UITextField!
    @IBOutlet weak var txfPhoneNumber: UITextField!
    @IBOutlet weak var lblRelationship: UILabel!
    @IBOutlet weak var pickerRelation: UIPickerView!
    @IBOutlet weak var txfEmail: UITextField!
    
    var dictSignUp = [String: AnyObject]()
    var pickerData = ""
    var arrRelation: NSMutableArray = ["Father", "Mother", "Guardian", "Other"]
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        lblRelationship.text = "Father"
        // Do any additional setup after loading the view.
        //pickerRelation.reloadAllComponents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Validation
    
    func isValidRegister() -> Bool {
        
        if !Util.isValidString(txfEmail.text!) {
            Util.showAlertWithMessage("Please enter email address", title: Key_Alert)
            return false
        } else if !Util.isValidEmail(txfEmail.text!) {
            Util.showAlertWithMessage("Please enter valid email address", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfFirstName.text!) {
            Util.showAlertWithMessage("Please enter first name", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfLastName.text!) {
            Util.showAlertWithMessage("Please enter last name", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfStreet.text!) {
            Util.showAlertWithMessage("Please enter street", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfCity.text!) {
            Util.showAlertWithMessage("Please enter city", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfState.text!) {
            Util.showAlertWithMessage("Please enter street", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfZip.text!) {
            Util.showAlertWithMessage("Please enter zip", title: Key_Alert)
            return false
        } else if !Util.isValidString(lblRelationship.text!) {
            Util.showAlertWithMessage("Please enter relationship", title: Key_Alert)
            return false
        }
        return true
    }

    // MARK: - Action Method
    
    /*"{
    ""FirstName"":""John"",
    ""LastName"":""Doe"",
    ""Street"":""100 Main Street"",
    ""City"":""Bel Air"",
    ""State"":""MD"",
    ""PostalCode"":""21015"",
    ""CellPhone"":""3013706303"",
    ""Pin"":""12345678"", ""IsMobile"": true,
    ""StudentPeerAccounts"":[{""Relationship"":""Parent"",""StudentId"":""34393""}],
    ""NotificationOptions"":[""AbsentAlert"", ""SuspensionAlert""]
}"*/
    
    @IBAction func btnNext_Action(_ sender: Any) {
        if isValidRegister() {
            
            let value = txfPhoneNumber.text?.replacingOccurrences(of: "-", with: "")
            
            dictSignUp["FirstName"]     = txfFirstName.text as AnyObject
            dictSignUp["LastName"]      = txfLastName.text as AnyObject
            dictSignUp["Street"]        = txfStreet.text as AnyObject
            dictSignUp["City"]          = txfCity.text as AnyObject
            dictSignUp["State"]         = txfState.text as AnyObject
            dictSignUp["PostalCode"]    = txfZip.text as AnyObject
            dictSignUp["CellPhone"]     = value as AnyObject
            dictSignUp["IsMobile"]      = true as AnyObject
            
            let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "SearchStdSclAddVC") as! SearchStdSclAddVC
            nextControll.dictSignUp  = dictSignUp
            nextControll.strRelation = lblRelationship.text!
            self.navigationController?.pushViewController(nextControll, animated: true)
        }
    }
    
    @IBAction func btnSelectRelation_Action(_ sender: Any) {
        
        UIView.animate(withDuration: 0.6, animations: {
            self.viewPicker.alpha        = 1.0
            //self.viewBackground.alpha    = 0.0
            
        }, completion: { finish in
            //self.viewBackground.isHidden = true
            self.viewPicker.isHidden     = false
        })
    }
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelPicker_Action(_ sender: Any) {
        
        UIView.animate(withDuration: 0.6, animations: {
            self.viewPicker.alpha        = 0.0
            //self.viewBackground.alpha    = 0.0
            
        }, completion: { finish in
            //self.viewBackground.isHidden = true
            self.viewPicker.isHidden     = true
        })
    }
    @IBAction func btnDonePicker_Action(_ sender: Any) {
        
        UIView.animate(withDuration: 0.6, animations: {
            self.viewPicker.alpha        = 0.0
            //self.viewPicker.alpha    = 0.0
            
        }, completion: { finish in
            //self.viewBackground.isHidden = true
            self.viewPicker.isHidden     = true
        })
        
        lblRelationship.text = "\(arrRelation[pickerRelation.selectedRow(inComponent:0)] as AnyObject)"
        //lblRelationship.text = strAge
        //isEdited = true
    }
}

//****************************************************
// MARK: - UIPickerViewDataSource and Delegate
//****************************************************

extension SignUpVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return arrRelation.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return arrRelation[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel!
        
        if  label == nil {
            label = UILabel()
        }
        
        pickerData = "\(arrRelation[row] as AnyObject)"
        
        //let title = NSAttributedString(string: pickerData, attributes: [NSFontAttributeName: UIFont(name: AppFontBook, size: 14)!])
        label?.text = "\(pickerData)"
        label?.textAlignment = .center
        label?.backgroundColor = UIColor.clear
        return label!
    }
}
