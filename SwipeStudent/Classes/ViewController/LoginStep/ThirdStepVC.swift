//
//  FirstStepVC.swift
//  SwipeK Teacher
//
//  Created by mac on 22/10/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class ThirdStepVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionYes(_ sender: Any) {
        appDelegate.isLoginTrue = true
        let vc  = storyboard?.instantiateViewController(withIdentifier: "FifthStepVC") as! FifthStepVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func actionNo(_ sender: Any) {
     let vc  = storyboard?.instantiateViewController(withIdentifier: "FourthStepVC") as! FourthStepVC
     self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
