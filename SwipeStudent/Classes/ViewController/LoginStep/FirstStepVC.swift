//
//  FirstStepVC.swift
//  SwipeK Teacher
//
//  Created by mac on 22/10/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class FirstStepVC: UIViewController {
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    var count = 0
    var arrQuestion = [JSON]()
    var isFail : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
appDelegate.isLoginTrue = false
        // Do any additional setup after loading the view.
        SurveyQuestionAPI()
    }
    

    @IBAction func actionYes(_ sender: Any) {
        appDelegate.isLoginTrue = true
       // let vc  = storyboard?.instantiateViewController(withIdentifier: "FifthStepVC") as! FifthStepVC
             //         self.navigationController?.pushViewController(vc, animated: true)
        questionArr("Y")
    }
    @IBAction func actionNo(_ sender: Any) {
        questionArr("N")
     /*   let vc  = storyboard?.instantiateViewController(withIdentifier: "SecondStepVC") as! SecondStepVC
               self.navigationController?.pushViewController(vc, animated: true)*/
    }
    
    
    func SurveyQuestionAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
       
        let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey")
        
        Networking.performApiCall(Networking.Router.Survey("\(LoginSurvey!)"), callerObj: self, showHud:true) { (response) -> () in
            appDelegate.hideHUD(self.view)
            if response.result.isSuccess {
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    let jsonObj = JSON(result)
                    let data = jsonObj["Questions"].arrayValue
                    if data.count > 0{
                        self.arrQuestion = data
                        let obj =   self.arrQuestion.first
                        self.lblQuestion.text = obj?["Question"].stringValue
                    }
                }
            }
        }
    }
    
    func SurveyResultAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        appDelegate.showHUD("Loading", onView: self.view)

        let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey")
        let id = UserDefaults.standard.string(forKey: "Permissions")
        let userName = UserDefaults.standard.string(forKey: "UserName")
        
        
        var postParams: [String: AnyObject] =
            [
                "SurveyId"          : "\(LoginSurvey!)" as AnyObject,
                "StudentId"      : id as AnyObject,
                "SubmitBy"         : userName as AnyObject
        ]
        if isFail{
            postParams["SurveyResult"] = "Fail" as AnyObject
        }else{
            postParams["SurveyResult"] = "Pass" as AnyObject
        }
        print("Post Parameter is:\(postParams)")
        Networking.performApiCall(Networking.Router.SurveyResult(postParams), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                if self.isFail{
                    let vc  = self.storyboard?.instantiateViewController(withIdentifier: "FifthStepVC") as! FifthStepVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let vc  = self.storyboard?.instantiateViewController(withIdentifier: "SecondStepVC") as! SecondStepVC
                        self.navigationController?.pushViewController(vc, animated: true)
                }
                
            } else {
                Util.showAlertWithMessage("Password Does not match", title: "Error")
                
            }
        }
    }
    
    
    
    
    func questionArr(_ userAnswer : String){
        if arrQuestion.count > 0{
            let obj =   arrQuestion.first
            let answer = obj?["ExpectedAnswer"].stringValue
            if userAnswer != answer{
                isFail  = true
                SurveyResultAPI()
                return
            }
            arrQuestion.removeFirst()
         //  print( arrQuestion.count)
            if arrQuestion.count > 0{
          let obj =   arrQuestion.first
            lblQuestion.text = obj?["Question"].stringValue
            }else{
                print("No data2")
                SurveyResultAPI()
            }
        }else{
            print("No data")
            SurveyResultAPI()
        }
        print(isFail)
    }
    
}
