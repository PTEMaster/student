//
//  FirstStepVC.swift
//  SwipeK Teacher
//
//  Created by mac on 22/10/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SecondStepVC: UIViewController {
    // MARK: - Properties
    
    @IBOutlet weak var lblStudent: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var ImgQRCode: UIImageView!
    
    
    var arrStudentLogin : StudentHome!
    var strPersonId = ""
    var strStudentId = ""
    var strSchoolId = ""
    var strGranted = ""
    var strStudentIntId = ""
    //var strPersonId = ""
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
          strPersonId = UserDefaults.standard.string(forKey: "PersonId")!
        self.navigationItem.hidesBackButton = true
        print(Date.getCurrentDate())
        lblDate.text = Date.getCurrentDate()
        NotificationSubscribeAPI()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK: - QR Code
    
    func generateBarcode(from string: String , nameBarCode:String) -> UIImage? {
         let data = string.data(using: String.Encoding.ascii)

         if let filter = CIFilter(name: nameBarCode) {
             filter.setValue(data, forKey: "inputMessage")
            //filter.setValue("H", forKey: "inputCorrectionLevel")
             let transform = CGAffineTransform(scaleX: 10, y: 10)

             if let output = filter.outputImage?.transformed(by: transform) {
                 return UIImage(ciImage: output)
             }
         }

         return nil
     }
    
    func fromString(string : String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let output = filter?.outputImage?.transformed(by: transform)
        return UIImage(ciImage: output!)
    }
    
    // MARK: - Action Method

    @IBAction func btnLogout_Action(_ sender: Any) {
        
       let alertController = UIAlertController(title: nil, message: "Are you sure do you want to logout?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (actionlogout) in
            UserDefaults.standard.set("false", forKey: "Loggedin")
            UserDefaults.standard.logoutIsLogin()
            UserDefaults.standard.logoutTouchId()
            UserDefaults.standard.logoutData()
            UserDefaults.standard.removeObject(forKey: "Permissions")
            UserDefaults.standard.removeObject(forKey: "LoginSurvey")
            UserDefaults.standard.removeObject(forKey: "BioDate")
 
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.setViewControllers([vc], animated: true)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)

    }
    
    @IBAction func btnStudentDetail_Action(_ sender: Any) {
        
        let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "StudentDetailsVC") as! StudentDetailsVC
        nextControll.strGranted  = strGranted
        nextControll.strSchoolId = strSchoolId
        nextControll.strStudentNumber = strStudentId
        nextControll.strIntStdId = strStudentIntId
        
        self.navigationController?.pushViewController(nextControll, animated: true)
    }
    
    @IBAction func actionYes(_ sender: Any) {
        appDelegate.isLoginTrue = true
        let vc  = storyboard?.instantiateViewController(withIdentifier: "StudentHomeVC") as! StudentHomeVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // MARK: - API Method
    
    func NotificationSubscribeAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        let postParams: [String: AnyObject] =
            [
                "RecipientId"          : strPersonId as AnyObject,
        ]
        
        print("Post Parameter is:\(postParams)")
        
        Networking.performApiCall(Networking.Router.NotificationSubscribe(postParams), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    print(jsonObj)
                    
                    let data = jsonObj["data"][0]
                    print(data)
                    if (data != nil) {
                        self.strSchoolId = data["SchoolId"].stringValue
                        self.strStudentId = data["StudentNumber"].stringValue
                        self.strStudentIntId = data["StudentId"].stringValue
                        self.strGranted = data["Granted"].stringValue
                        self.StudentProfileAPI()
                    }
                }
            }
        }
    }
    
    func StudentProfileAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        Networking.performApiCall(Networking.Router.StudentProfile(strSchoolId, strStudentId), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    let jsonObj = JSON(result)
                    print(jsonObj)
                    
                    let data = jsonObj["data"]
                    print(data)
                    if (data != nil) {
                        
                        
                        let strimg = "https://\(data["ImageHost"].stringValue)\(data["ImageUrl"].stringValue)"
                        
                        if let url = URL(string: strimg) {
                            self.imgProfile.kf.indicatorType = .activity
                            self.imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgDefault"), options: nil, progressBlock: nil, completionHandler: nil)
                        }
                                          
                        
                        let strName = "\(data["FirstName"].stringValue) \(data["MiddleName"].stringValue) \(data["LastName"].stringValue)"
                        self.lblStudent.text = strName
                        
                        let strBarCode = "\(data["StudentNumber"].stringValue)"
                        print(strBarCode)
                       self.ImgQRCode.image = self.generateBarcode(from: strBarCode, nameBarCode: "CIQRCodeGenerator")
                    //  self.imgBarCode.image = self.generateBarcode(from: strBarCode, nameBarCode: "CICode128BarcodeGenerator")
                        
                        
                    }
                }
            }
        }
    }

}


extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "MM/dd/yy"

        return dateFormatter.string(from: Date())

    }
}
