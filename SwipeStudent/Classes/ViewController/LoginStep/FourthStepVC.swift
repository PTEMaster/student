//
//  FirstStepVC.swift
//  SwipeK Teacher
//
//  Created by mac on 22/10/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class FourthStepVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionYes(_ sender: Any) {
        let vc  = storyboard?.instantiateViewController(withIdentifier: "FifthStepVC") as! FifthStepVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func actionNo(_ sender: Any) {
        if  appDelegate.isLoginTrue == true{
                   UserDefaults.standard.logoutData()
                   UserDefaults.standard.logoutIsLogin()
                   UserDefaults.standard.logoutTouchId()
            let vc  = storyboard?.instantiateViewController(withIdentifier: "FifthStepVC") as! FifthStepVC
            self.navigationController?.pushViewController(vc, animated: true)
               }else{
            let vc  = storyboard?.instantiateViewController(withIdentifier: "StudentHomeVC") as! StudentHomeVC
                self.navigationController?.pushViewController(vc, animated: true)
                   
               }
    
    }
    
}
