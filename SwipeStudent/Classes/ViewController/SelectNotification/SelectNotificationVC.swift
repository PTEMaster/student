//
//  SelectNotificationVC.swift
//  SwipeStudent
//
//  Created by mac on 28/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

// http://stackoverflow.com/a/30724543
extension Array where Element : Equatable {
    mutating func removeObject(object : Iterator.Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
}

class SelectNotificationVC: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGrade: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    var arrIndexs = (0...6).map({ $0 })
    
    var dictSignUp : [String: AnyObject] = [:]
    var arrNotificationads = NSArray()
    var arrSendArray : NSMutableArray = []
    var arrMain  : NSDictionary = [:]
    var strId  = ""
    let arrStudentPeer = NSMutableArray()
    var dictMain : [String: AnyObject] = [:]
    var strName = ""
    var strGrad = ""
    
    //var contents : NSMutableArray = []
    var selectedIndexPaths = [NSIndexPath]()
    
    var arrNotiFication = ["School Arrival", "School Tardy", "School Absence", "Consequence Issued", "Class Cuts", "Fines", "All Alerts"]
    
    var contents = ["School Arrival", "School Tardy", "School Absence", "Consequence Issued", "Class Cuts", "Fines", "All Alerts"]
    
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(dictSignUp)
        NotificationListAPI()
        lblName.text  = strName
        lblGrade.text = strGrad
        let viewBg = UIView()
        viewBg.backgroundColor = UIColor.white
        
        tblNotification.backgroundView = viewBg
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Action Method
    
    @IBAction func btnBack_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddAnatherStudent_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnFinish_Action(_ sender: Any) {
        self.SignUpAPI()
    }
    
    // MARK: - API Methods
     
     func SignUpAPI() {
         
         if (!isNetworkAvailable) {
             Util.showNetWorkAlert()
             return
         }
         
         appDelegate.showHUD("Loading", onView: self.view)
         
         self.dictSignUp["device_token"] = appDelegate.strDeviceToken as AnyObject
         self.dictSignUp["end_point"] = appDelegate.strEndPointAWS as AnyObject
         
         Networking.performApiCall(Networking.Router.signUp(dictSignUp), callerObj: self, showHud:true) { (response) -> () in
             appDelegate.hideHUD(self.view)
             if response.result.isSuccess {
                 
                 //** Json Parsing: using SwiftyJSON library
                 if let result = response.result.value {
                     
                     var jsonObj = JSON(result)
                     
                     print(jsonObj)
                     
                     let data = "\(jsonObj["responseText"])"
                     
                     let refreshAlert = UIAlertController(title: Key_Alert, message: data, preferredStyle: UIAlertControllerStyle.alert)
                     
                     refreshAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                         print("Handle Cancel Logic here")
                
                         UserDefaults.standard.set("true", forKey: "Loggedin")
                         
                         let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                         self.navigationController?.setViewControllers([vc], animated: true)
                     }))
                     
                     self.present(refreshAlert, animated: true, completion: nil)
                 }
             }
         }
     }
     


    
    // MARK: - API Methods
    
    func NotificationListAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)
        
        Networking.performApiCall(Networking.Router.NotificationList(), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    print(jsonObj)
                    
                    let data = jsonObj["data"]
                    print(data)
                    if (data != nil) {
                        self.arrMain = data.dictionary! as NSDictionary
                        print(self.arrMain)
                        let kjh = self.arrMain.allValues
                        self.arrNotificationads = kjh as NSArray
                        print(self.arrNotificationads)
                        self.arrNotiFication.removeAll()
                        
                        for value in self.arrNotificationads {
                            
                            let ljh: String = String(describing: value)
                            
                            print(ljh)
                            
                            self.arrNotiFication.append(ljh)
                        }
                        
                     
                        self.dictMain["Relationship"] = "self" as AnyObject
                        self.dictMain["StudentId"] = self.strId as AnyObject
                        self.dictMain["NotificationOptions"] = self.arrNotiFication as AnyObject
                        print(self.dictMain)
                    
                        self.arrStudentPeer.add(self.dictMain)
                        self.dictSignUp["StudentPeerAccounts"] = self.dictMain as AnyObject
                        print(self.dictSignUp)
                        self.tblNotification.reloadData()
                    }
                }
            }
        }
    }
    
}

//****************************************************
// MARK: - UITableViewDataSource and Delegate
//****************************************************

extension SelectNotificationVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotificationads.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tblNotification.dequeueReusableCell(withIdentifier: "SelectNotiCell") as? SelectNotiCell
        
        if cell == nil {
            cell = SelectNotiCell(style: UITableViewCellStyle.default, reuseIdentifier: "SelectNotiCell")
        }
        cell?.lblName.text =  "\(arrNotificationads[indexPath.row])"
        
        if arrIndexs.index(of: indexPath.row) != nil {
            // selected condition
            cell?.imgSelect.image = #imageLiteral(resourceName: "checked")
        } else {
            // unselected
            cell?.imgSelect.image = #imageLiteral(resourceName: "unchecked")
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrIndexs.index(of: indexPath.row) != nil {
            arrIndexs.removeObject(object: indexPath.row)
            arrNotiFication.removeObject(object: "\(arrNotificationads[indexPath.row])")
            print(arrNotiFication)
        } else {
            
            arrIndexs.append(indexPath.row)
            arrNotiFication.append("\(arrNotificationads[indexPath.row])")
            print(arrNotiFication)
            
        }
        tblNotification.reloadData()
        
       /* if (selectedIndexPaths.contains(indexPath as NSIndexPath))
        {
            selectedIndexPaths.removeObject(object: indexPath as NSIndexPath)
            print("enterrrrrrr")
            let str = arrNotificationads[indexPath.row]
            arrNotiFication.append("\(arrNotificationads[indexPath.row])")
            self.tblNotification.reloadData()
            print(self.arrNotiFication.count)
            
            print(arrNotiFication)
        }
        else
        {
            selectedIndexPaths.append(indexPath as NSIndexPath)
            print("outerrrrr")
            let str = arrNotificationads[indexPath.row]
            arrNotiFication.removeObject(object: "\(arrNotificationads[indexPath.row])")
            tblNotification.reloadData()
            print(arrNotiFication.count)
            print(arrNotiFication)
        }*/
 }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        print(arrNotiFication[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
/* //        if (selectedIndexPaths.contains(indexPath as NSIndexPath))
 //        {
 //            selectedIndexPaths.removeObject(object: indexPath as NSIndexPath)
 //
 //            var lkj = self.arrNotiFication
 //
 //            contents = [lkj.remove(at: indexPath.row)]
 //
 //            print(contents)
 //
 //        }
 //        else
 //        {
 //            selectedIndexPaths.append(indexPath as NSIndexPath)
 //
 //            //var lkj : [String] = self.arrNotiFication
 //            contents.append(self.arrNotiFication[indexPath.row])
 //           // contents = lkj.append(self.arrNotiFication[indexPath.row])
 //            print(contents)
 //        }
 ////
 ////        let cell = tableView.cellForRow(at: indexPath as IndexPath)!
 ////        populateCell(cell: cell, indexPath: indexPath as NSIndexPath )*/
}
