//
//  PasswordLoginViewController.swift
//  SmileLock-Example
//
//  Created by rain on 4/22/16.
//  Copyright © 2016 RECRUIT LIFESTYLE CO., LTD. All rights reserved.
//

import UIKit
import SmileLock

class PasswordLoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtfPassword: UITextField!
    @IBOutlet weak var lblPinEnter: UILabel!
    @IBOutlet weak var txfCellNumber: PhoneFormattedTextField!
    
    //MARK: Property
    var passwordContainerView: PasswordContainerView!
    let kPasswordDigit = 4
    var strEnterPassword = ""
    var StrConfirmPassword = ""
    var isLogin = false

    
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txfCellNumber.text = UserDefaults.standard.string(forKey: "CellNumber")
        
        if txfCellNumber.text == "" {
            txfCellNumber.placeholder = "Email"
            txfCellNumber.becomeFirstResponder()
        } else {
            txfCellNumber.becomeFirstResponder()
        }

    }
    
    @IBAction func btnBack_Acion(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLogin_Action(_ sender: Any) {
        
        if isLogin == true {
            LoginAPI()
        } else {
            Util.showAlertWithMessage("Please Enter Valid Info", title: "")
        }
    }
    
    func defaultExample() {
        txfCellNumber.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "###-###-####")
    }
 
    func LoginAPI() {
        
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        appDelegate.showHUD("Loading", onView: self.view)

        let value = self.txfCellNumber.text?.replacingOccurrences(of: "-", with: "")
        
        let postParams: [String: AnyObject] =
            [
                "userName"          : value as AnyObject,
                "password"          : txtfPassword.text as AnyObject,
                "device_token"      : appDelegate.strDeviceToken as AnyObject,
                "end_point"         : appDelegate.strEndPointAWS as AnyObject
        ]
        
        print("Post Parameter is:\(postParams)")
        
        Networking.performApiCall(Networking.Router.Login(postParams), callerObj: self, showHud:true) { (response) -> () in
            
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                
                //** Json Parsing: using SwiftyJSON library
                if let result = response.result.value {
                    
                    var jsonObj = JSON(result)
                    
                    print(jsonObj)
                    
                    
                    let Permissions = jsonObj["Permissions"].arrayValue
                    let lName = jsonObj["Person"].stringValue
                    
                    let user = jsonObj["UserName"].stringValue
                    print(Permissions)
                    let id = Permissions[0].stringValue
                    UserDefaults.standard.set(id, forKey: "Permissions")
                    
                    UserDefaults.standard.set(user, forKey: "UserName") //setObject
                    
                    UserDefaults.standard.set(lName, forKey: "PersonId") //setObject
                    
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                    let str = df.string(from: Date())
                    UserDefaults.standard.setValue(str, forKey: "BioDate")
                    
                    UserDefaults.standard.set(self.txfCellNumber.text, forKey: "CellNumber")
                    
                    UserDefaults.standard.set("true", forKey: "Loggedin")
                    
                    UserDefaults.standard.setTouchid(value: true)
                    UserDefaults.standard.setLoggedIn(value: true)
                    
                    let LoginSurvey = jsonObj["LoginSurvey"].intValue
                    UserDefaults.standard.set("\(LoginSurvey)", forKey: "LoginSurvey") //setObject
                  //39455@swipek12.com
                    if user == "kaa004@swipek12.com"{
                        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "StudentHomeVC") as! StudentHomeVC
                            self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        if LoginSurvey > 0{
                            let nextControll = self.storyboard?.instantiateViewController(withIdentifier: "FirstStepVC") as! FirstStepVC
                            self.navigationController?.pushViewController(nextControll, animated: true)
                        }else{
                            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "StudentHomeVC") as! StudentHomeVC
                                self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            } else {
                Util.showAlertWithMessage("Password Does not match", title: "Error")
                
            }
        }
    }
  
}
