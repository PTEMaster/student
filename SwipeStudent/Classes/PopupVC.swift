//
//  PopupVC.swift
//  SwipeStudent
//
//  Created by Himanshu Pal on 14/06/22.
//  Copyright © 2022 mac. All rights reserved.
//

import UIKit

class PopupVC: UIViewController {
    
    @IBOutlet weak var viewpopup: UIView!
    @IBOutlet weak var lblActiveInactive: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblEnter: UILabel!
    @IBOutlet weak var lblLeave: UILabel!
    @IBOutlet weak var lblLevaeTitle: UILabel!
    var dict = [String: Any]()
    var strimg = ""
    let notificationCenter = NotificationCenter.default
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)

            
    }
    @objc func appMovedToBackground() {
        // do whatever event you want
        print("here is background goes application:- ")
        self.dismiss(animated: false, completion: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        notificationCenter.removeObserver(self)
    }
    func setData() {
        print(dict)
      let activeStatus =   dict["active"] as? Int
        
        if activeStatus == 0{
            // Red
            //viewpopup.backgroundColor = UIColor.red
            self.lblActiveInactive.text = "Hall Pass Expired"
            
        }else{
            //Green
            viewpopup.backgroundColor = UIColor.green
            self.lblActiveInactive.text = "Hall Pass Active"
            
        }
        
        
       
        self.lblLocation.text =   dict["Room"] as? String
        self.lblEnter.text = dict["TimeString"] as? String
        self.lblLeave.isHidden = dict["leaveTime"] as? String == "" ? true : false
        self.lblLevaeTitle.isHidden = dict["leaveTime"] as? String == "" ? true : false
        if dict["leaveTime"] as? String != "" {
            self.lblLeave.text = dict["leaveTime"] as? String
        }
        
        
        if let url = URL(string: strimg) {
            self.imgProfile.kf.indicatorType = .activity
            self.imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgDefault"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
   
    @IBAction func actionClose(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

extension StudentHomeVC {
    //dict["expiresAsString"] as? String ?? ""
    func diffrenceTime(time2Str: String)  {
        
        let dateFormatter : DateFormatter = DateFormatter()
        //  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "hh:mm:ss a"
        dateFormatter.timeZone = TimeZone(abbreviation: "EDT")
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        let timeformatter = DateFormatter()
        timeformatter.timeZone = TimeZone(abbreviation: "EDT")
        timeformatter.dateFormat = "hh:mm:ss a"
        let timeformatter2 = DateFormatter()
        timeformatter2.timeZone = TimeZone(abbreviation: "EDT")
        timeformatter2.dateFormat = "hh:mm a"
        
        guard let time1 = timeformatter.date(from: dateString),
              let time2 = timeformatter2.date(from: time2Str) else { return  }
        //You can directly use from here if you have two dates
        
        let interval = time2.timeIntervalSince(time1)
        let ti = NSInteger(interval)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        //let hours = (ti / 3600)
        print("\(minutes):\(seconds)")
        let time = "\(minutes):\(seconds)"
        let arr = (time ?? "00:00").components(separatedBy: ":")
        
        if arr.count > 1 {
            //let hours = Int(arr[0].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
            let mins = Int(arr[0].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
            let sec = Int(arr[1].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
            let timeSeconds = Double(mins * 60) + Double(sec)
            let timeOut = Int(timeSeconds)
            print( Int(timeSeconds))
            if timeOut <= 0 {
                self.btnHallPass.isHidden = true
                TimerModal.sharedTimer.stopTimer()
            }
        }
    }
    
}
