//
//  ApiService.swift
//  CommunitySafetyWatch
//
//  Created by ABC on 4/17/18.
//  Copyright © 2018 ABC. All rights reserved.
//

import Foundation
import UIKit.UIImage

// Configure your base URL.
///let BaseURL = "https://evergreen.swipek12.com"




// API paths.
public enum RequestPath: String {
    
    case searchAPI = "/dataservice/School/Search"
    
    case LoginApi = "Ios/login"
    case SignupApi = "Ios/signup"
    case SocialLogin = "Ios/social_login"
    case getProfile = "Ios/getProfile"
    case editProfile = "Ios/updateProfile"
    case changePassword = "home/changePassword"
    case forgetPassowrd = "home/forgetPassword"
    case resetPassowrd = "home/resetPassword"
    
    // ****** Video Compitions****** //
    case videoCompitionsFeed =  "Video_competition/newsfeed"
    case addVideo = "Video_competition/addVideo"
    case videoLikeUnlike = "Video_competition/likeUnlike"
    case sendVideoComment = "Video_competition/comment"
    case getVideoComment = "Video_competition/getComment"
    case reviewSend = "Video_competition/rating"
    case ratingList = "Video_competition/getRating"
    
    /// ****** post comment ****////
    case getPostComment = "Api/getComment"
    case sendPostcomment = "Api/comment"
    
    
    case notification = "Ios/notification"
    case getFriendRequest = "Ios/getFreindRequest"
    
    //****** Post List *******//
    case doctorList = "Api/doctorList"
    case postListNewzFeed = "Api/newsfeed"
    case addPost = "Api/addPost"
    case likeUnlikePost = "Ios/likeUnlike"

    case resouceList = "Ios/resourceList"
    case sponsorList = "Ios/sponsorList"
    case contactUs = "Ios/contactUs"
    case about = "home/aboutUs"
    
    case searchUser = "Api/searchUser"
    case friendRequest = "Api/friendRequest"
    
    /////// ****** ////
    case chatList = "Api/chatlist"
    case friendList = "Api/friendList"
    case chatHistory = "Api/chatHistory"
    case messageSend = "Api/chat"
 
    
    case logout = "Ios/logout"
    case howItWork = "assets/POSITIVEAPPINSTRUCTIONS.pdf"
    case report = "Api/report"

}

public enum HTTPMethod: String {
    case get       = "GET"
    case post     = "POST"
    case put       = "PUT"
    case patch   = "PATCH"
    case delete   = "DELETE"
}

public enum HTTPStatusCode:Int {
    case success = 200
    case created = 201
    case redirectionError = 301
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    case internalServerError = 500
    case methodNotImplemented = 501
    case gatewayTimeOut = 503
    case requestTimeout = -1001
    case internetOffline = -1009
    case requestLoadingFailed = -1005
    
}

public typealias HTTPParameters = [String: Any]

private let kNSSessionErrorDomain = Bundle.main.bundleIdentifier!

class NSSession: NSObject {
    
    var Timestamp: String {
        return "\(NSDate().timeIntervalSince1970 * 1000)"
    }
    
    static let shared: NSSession = {
        
        var instance = NSSession()
        let urlconfig = URLSessionConfiguration.default
        urlconfig.timeoutIntervalForRequest = 45 // For get Response for server
        urlconfig.timeoutIntervalForResource = 60 // For download any rescources {}
        instance.urlSession = Foundation.URLSession(configuration: urlconfig, delegate: nil, delegateQueue: OperationQueue.main)
        return instance
    }()
    
    class var DefaultFatalCodes: [HTTPStatusCode] {
        get {
            return [.badRequest, .internalServerError, .methodNotImplemented, .notFound, .unauthorized, .forbidden]
        }
    }
    
    private var urlSession: URLSession!
    private lazy var formDataQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Form data generator queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    // MARK: Public
    public func requestWith(path: String, method: HTTPMethod, parameters: HTTPParameters?, retryCount:Int, showHud: Bool, fatalStatusCodes:[HTTPStatusCode], shouldCacheResponse: Bool = false, completionHandler: @escaping(Bool, Data?, Error?) -> Void) -> Void {
        
        let request = enqueueRequestWith(path: path, method: method, parameters: parameters, cacheResponse: shouldCacheResponse)
        
        // Show Hud.
        if showHud {
           // Loader.showLoader(self.getLoaderTitle(withPath: path))
        }
        
        enqueueDataTaskWith(request: request, retryCount: retryCount, fatalStatusCodes: fatalStatusCodes) { (data, response, error) in
            
            // Hide Hud.
            
            self.handle(data: data, response: response, error: error, completionHandler: { (success, data, error) in
                
                if showHud {
                  //  Loader.hideLoader()
                }
                
                completionHandler(success, data, error)
            })
        }
    }
    
    
    public func multipartRequestWith(path: String, method: HTTPMethod, parameters: [HTTPParameters]?, images:[String: [UIImage]]?, retryCount:Int, showHud: Bool, fatalStatusCodes:[HTTPStatusCode], completionHandler: @escaping(Bool, Data?, Error?) -> Void) -> Void {
        
        var urlString = kAPI_BaseURL + path
        urlString = encode(url: urlString)
        
        let url = URL(string: urlString)!
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        //request.timeoutInterval = 30
        let boundary = "Boundary-\(UUID().uuidString)"
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
       // request.setValue("NjUxNTYxNDU2", forHTTPHeaderField: "Authorization")

        // Set access token in request header
      /////////  if userDefault.bool(forKey: kIsUserLoggedIn) {
            // print(LoggedInUser.sharedUser.access_token)
            // request.setValue(LoggedInUser.sharedUser.access_token, forHTTPHeaderField: "access-token")
     /// //////  }
        
        // Show Hud.
        if showHud {
           // Loader.showLoader(self.getLoaderTitle(withPath: path))
        }
        
        createMultipartFormData(boundary: boundary, parameters: parameters, images: images) { (data) in
            self.enqueueUploadTaskWith(request: request, data: data, retryCount: retryCount, fatalStatusCodes: fatalStatusCodes) { (data, response, error) in
                
                // Hide Hud.
                if showHud {
                    //Loader.hideLoader()
                }
                
                self.handle(data: data, response: response, error: error, completionHandler: { (success, data, error) in
                    completionHandler(success, data, error)
                })
            }
        }
    }
    
    func multipartNewRequestWith(path: String, method: HTTPMethod, parameters: [String: String]?, image: UIImage?, imageParam: String, retryCount: Int, showHud: Bool, fatalStatusCodes:[HTTPStatusCode], completionHandler: @escaping(Bool, Data?, Error?) -> Void) {
        let urlString = kAPI_BaseURL + path
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = method.rawValue
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("NjUxNTYxNDU2", forHTTPHeaderField: "Authorization")
        
        var data: Data? = nil
        if image != nil {
            data = UIImageJPEGRepresentation(image!, 0.7)
        }
        
        
        request.httpBody = createBodyForMultipartData(parameters: parameters ?? [:], boundary: boundary, data: data, imageParam: imageParam, mimeType: "image/jpg", filename: "test.jpg")
        
        // Show Hud.
        if showHud {
          //  Loader.showLoader(self.getLoaderTitle(withPath: path))
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if let code = HTTPStatusCode(rawValue: httpResponse.statusCode) {
                    
                    if retryCount > 0, !(fatalStatusCodes.contains(code)), code != .success, code != .created {
                        self.multipartNewRequestWith(path: path, method: method, parameters: parameters, image: image, imageParam: imageParam, retryCount: retryCount, showHud: showHud, fatalStatusCodes: fatalStatusCodes, completionHandler: completionHandler)
                    }
                    else {
                        self.handle(data: data, response: response, error: error, completionHandler: { (success, data, error) in
                            DispatchQueue.main.async {
                                if showHud {
                                  //  Loader.hideLoader()
                                }
                                completionHandler(success, data, error)
                            }
                        })
                    }
                }
                else {
                    self.handle(data: data, response: response, error: error, completionHandler: { (success, data, error) in
                        DispatchQueue.main.async {
                            if showHud {
                              //  Loader.hideLoader()
                            }
                            completionHandler(success, data, error)
                        }
                    })
                }
            }
            
            
        }.resume()
        /*
        enqueueDataTaskWith(request: request, retryCount: retryCount, fatalStatusCodes: fatalStatusCodes) { (data, response, error) in
            
            // Hide Hud.
            
            self.handle(data: data, response: response, error: error, completionHandler: { (success, data, error) in
                
                if showHud {
                    Loader.hideLoader()
                }
                
                completionHandler(success, data, error)
            })
        }
 */
    }
    
    public func cancelAllRequests() -> Void {
        
        urlSession.getAllTasks { (tasks) in
            for task in tasks {
                task.cancel()
            }
        }
    }
    
    public func cancelRequestWith(path: String) -> Void {
        
        urlSession.getAllTasks { (tasks) in
            for task in tasks {
                if let absoluteURL = task.originalRequest?.url?.absoluteString {
                    if absoluteURL.contains(path) {task.cancel(); break}
                }
            }
        }
    }
    
    // MARK: Private
    private func enqueueDataTaskWith(request: URLRequest, retryCount:Int, fatalStatusCodes:[HTTPStatusCode], completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let task = urlSession.dataTask(with: request, completionHandler: { data, response, error in
            //print(" response \(String(describing: response))")
            if let httpResponse = response as? HTTPURLResponse {
                
                if let code = HTTPStatusCode(rawValue: httpResponse.statusCode) {
                    
                    if retryCount > 0, !(fatalStatusCodes.contains(code)), code != .success, code != .created {
                        self.enqueueDataTaskWith(request: request, retryCount: retryCount-1, fatalStatusCodes: fatalStatusCodes, completionHandler: completionHandler)
                    }
                    else {
                        completionHandler(data, response, error)
                    }
                }
                else {
                    completionHandler(data, response, error)
                }
            }
        })
        
        task.resume()
    }
    
    private func enqueueUploadTaskWith(request: URLRequest, data:Data, retryCount:Int, fatalStatusCodes:[HTTPStatusCode], completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let uploadTask = urlSession.uploadTask(with: request, from: data) { (responseData, response, error) in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if let code = HTTPStatusCode(rawValue: httpResponse.statusCode) {
           
                    // change for multipart
                    guard code == .success/*.requestLoadingFailed */, retryCount > 0 else {
                        completionHandler(nil, nil, NSError.errorWith(code: code.rawValue, localizedDescription: error!.localizedDescription))
                        return
                    }
                    
                    if retryCount > 0, !(fatalStatusCodes.contains(code)), code != .success, code != .created {
                        self.enqueueUploadTaskWith(request: request, data: data, retryCount: retryCount-1, fatalStatusCodes: fatalStatusCodes, completionHandler: completionHandler)
                    }
                    else {
                        completionHandler(responseData, response, error)
                    }
                }
                else {
                    completionHandler(responseData, response, error)
                }
            }
        }
        uploadTask.resume()
    }
    
    private func handle(data: Data?, response:URLResponse?, error:Error?, completionHandler: @escaping (Bool, Data?, Error?) -> Void) -> Void {
        
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
        if let httpResponse = response as? HTTPURLResponse {
            
            //print("Validating request: \(String(describing: response?.url?.absoluteString)) with HTTP status code: \(httpResponse.statusCode)")
            if httpResponse.statusCode == HTTPStatusCode.unauthorized.rawValue {
                self.sessionExpiredAlert()
                return
            }
        }
        
        guard error == nil else {
            completionHandler(false, nil, error)
           // Loader.hideLoader()
            return
        }
        
        guard let data = data else {
            completionHandler(false, nil, nil)
            return
        }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                
                if json["recipes"] != nil || json["recipe"] != nil || json["recipe_tools"] != nil || json != nil {
                    completionHandler(true, data, nil)
                }
                    
                else {
                    let error = json["error"] as? [String:Any]
                    if error == nil {    return    }
                    // TODO: - Check APP Is crash if error is getting nil
                    if let errorMessage = error!["message"] as? String {
                        let responseError = NSError.errorWith(code: 0, localizedDescription: errorMessage)
                        completionHandler(false, nil, responseError)
                    }
                }
            }
        } catch let error {
            completionHandler(false, nil, error)
        }
    }
    
    private func sessionExpiredAlert() {
        let alertController = UIAlertController(title: "Session expired", message: "Your last session is expired, please login again to continue" , preferredStyle:UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Login", style: .cancel, handler: { action in
            
        }))
        
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: URLRequest
    private func enqueueRequestWith(path: String, method: HTTPMethod, parameters: HTTPParameters?, cacheResponse: Bool) -> URLRequest {
        
        let urlString = kAPI_BaseURL + path
        // urlString = encode(url: urlString)
            
        let url = URL(string: urlString)!
        print(urlString)
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = method.rawValue
        request.setValue("NjUxNTYxNDU2", forHTTPHeaderField: "Authorization")
        

        // request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        // Set access token in request header
       ////////// if userDefault.bool(forKey: kIsUserLoggedIn) {
            /* if let token  = LoggedInUser.sharedUser.access_token {
             print(token)
             request.setValue("\(token)", forHTTPHeaderField: "access-token")
             }*/
      ////////  }
        // Caching response
        if cacheResponse {
            request.cachePolicy = .returnCacheDataElseLoad
        }
        
        switch method {
        case .post:
            
            if let data = JSONEncode(url: url, parameters: parameters) {
                request.httpBody = data
                
                print(data)
            }
 
           /* if let jsonData = try? JSONSerialization.data(withJSONObject: parameters ?? [:]) {
                request.httpBody = jsonData
            }*/
            
        case .get:
            // return request as URLRequest
            if let encodedURL = URLEncode(url: url, parameters: parameters) {
                request.url = encodedURL
            }
            
        default:
            if let data = JSONEncode(url: url, parameters: parameters) {
                request.httpBody = data
            }
        }
        return request as URLRequest
    }
    
   
    
    
    private func createMultipartFormData(boundary: String, parameters: [HTTPParameters]?, images: [String: [UIImage]]?, completionHandler: @escaping (Data) -> Void) -> Void {
        
        let blockOperation = BlockOperation {
            
            var formData = Data()
            
            if parameters != nil, parameters!.count > 0 {
                
                for pairs in parameters! {
                    
                    for (key, element) in pairs {
                        // EDITED BY JAI------------ START
                        if let ele = element as? [String]{
                            for val in ele{
                                
                                formData.append("--\(boundary)\r\n".data(using: .utf8)!)
                                formData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                                formData.append("\(val)\r\n".data(using: .utf8)!)
                            }
                        }
                        else{
                            formData.append("--\(boundary)\r\n".data(using: .utf8)!)
                            formData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                            formData.append("\(element)\r\n".data(using: .utf8)!)
                        }
                        // EDITED BY JAI------------ END
                        /*formData.append("--\(boundary)\r\n".data(using: .utf8)!)
                        formData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                        formData.append("\(element)\r\n".data(using: .utf8)!)*/
                        
                    }
                }
            }
            
            let key = images?.keys.first
            print(key ?? "no key found")
            
            if images != nil, (images![key!]?.count)! > 0 {
                
                for value in (images?[key!]!)! {
                    
                    guard let imageData = UIImageJPEGRepresentation(value, 0.5) else { continue }
                    formData.append("--\(boundary)\r\n".data(using: .utf8)!)
                  //  formData.append("Content-Disposition: form-data; name=\"\(key!)\"; filename=\"\("image" + "\(self.Timestamp)").jpeg\"\r\n".data(using: .utf8)!)
                    formData.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
                    formData.append(imageData)
                    formData.append("\r\n".data(using: .utf8)!)
                }
            }
            formData.append("--\(boundary)--\r\n".data(using: .utf8)!)
            
            
            OperationQueue.main.addOperation {
                completionHandler(formData)
            }
        }
        
        formDataQueue.addOperation(blockOperation)
    }
    
    private func createBodyForMultipartData(parameters: [String: String], boundary: String, data: Data?, imageParam: String, mimeType: String, filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        for (key, value) in parameters {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        if data != nil {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(imageParam)\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimeType)\r\n\r\n")
            body.append(data!)
        }
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        return body as Data
    }
    
    
    // MARK: Encoding
    private func encode(url: String) -> String {
        return url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    }
    
    private func JSONEncode(url: URL, parameters: HTTPParameters?) -> Data? {
        
        guard parameters != nil else {
            return nil
        }
        
        guard !parameters!.isEmpty else {
            return nil
        }
        
        return query(parameters!).data(using: .utf8, allowLossyConversion: false)!
    }
    
    private func URLEncode(url: URL, parameters: HTTPParameters?) -> URL? {
        
        guard parameters != nil else {
            return nil
        }
        
        guard !parameters!.isEmpty else {
            return nil
        }
        
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false), !parameters!.isEmpty {
            let percentEncodedQuery = (urlComponents.percentEncodedQuery.map { $0 + "&" } ?? "") + query(parameters!)
            urlComponents.percentEncodedQuery = percentEncodedQuery
            return urlComponents.url
        }
        
        return nil
    }
    
    private func query(_ parameters: HTTPParameters) -> String {
        
        var components: [(String, String)] = []
        
        for key in parameters.keys.sorted(by: <) {
            let value = parameters[key]!
            components += queryComponents(fromKey: key, value: value)
        }
        
        return components.map { "\($0)=\($1)" }.joined(separator: "&")
    }
    
    private func queryComponents(fromKey key: String, value: Any) -> [(String, String)] {
        
        var components: [(String, String)] = []
        
        if let dictionary = value as? [String: Any] {
            for (nestedKey, value) in dictionary {
                components += queryComponents(fromKey: "\(key)[\(nestedKey)]", value: value)
            }
        }
        else if let array = value as? [Any] {
            for value in array {
                components += queryComponents(fromKey: "\(key)[]", value: value)
            }
        }
        else if let value = value as? NSNumber {
            if value.isBoolean {
                components.append((escape(key), escape((value.boolValue ? "1" : "0"))))
            }
            else {
                components.append((escape(key), escape("\(value)")))
            }
        }
        else if let bool = value as? Bool {
            components.append((escape(key), escape((bool ? "1" : "0"))))
        }
        else {
            components.append((escape(key), escape("\(value)")))
        }
        
        return components
    }
    
    private func escape(_ string: String) -> String {
        
        let generalDelimitersToEncode = ":#[]@"
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowedCharacterSet = CharacterSet.urlQueryAllowed
        allowedCharacterSet.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        
        return string.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? string
    }
    
    // MARK: Validate
    private func validate(response: HTTPURLResponse) -> Bool {
        //, contentType.contains("application/json")
        if (response.allHeaderFields["Content-Type"] as? String) != nil {
            return true
        }
        return false
    }
    
    func getLoaderTitle(withPath path: String) -> String? {
        switch path {
        case RequestPath.LoginApi.rawValue:
            return "Login in"
        case RequestPath.SignupApi.rawValue:
            return "Sign in"
        default:
            return nil
        }
    }
    
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}

extension NSNumber {
    fileprivate var isBoolean: Bool { return CFBooleanGetTypeID() == CFGetTypeID(self) }
}

extension NSError {
    static func errorWith(code: Int, localizedDescription: String?) -> NSError {
        return NSError(domain:kNSSessionErrorDomain, code:code, userInfo:[NSLocalizedDescriptionKey: localizedDescription != nil ? localizedDescription! : ""])
    }
}

