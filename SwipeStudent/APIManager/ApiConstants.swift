//
//  ApiConstants.swift
//  Bosala
//



//** Base url of web service
//TODO: Build Changes
//For Live
//creativethoughtsinfo.com/CT10/qareeb/Apis/images/users
//For Development
//let kAPI_BaseURL                = "http://www.creativethoughtsinfo.com/CT06/NGO/wp-json/"
let kAPI_BaseURL                  = "https://mobile-api.swipek12.com"
//"https://evergreen.swipek12.com"

//let kAPI_BaseURL = "https://ctinfotech.com/CT06/"

//dataservice/School/Search
let kUserimage_BaseURL                = "http://creativethoughtsinfo.com/CT10/qareeb/Apis/images/users/"

let kProviderimage_BaseURL                = "http://creativethoughtsinfo.com/CT10/qareeb/Apis/images/provider/"

//************************** Constant for API Keys **************************//

//** Common Api Constant
let kAPI_Data                           = "data"
let kAPI_Items                          = "items"
let kAPI_ServerDateFormat               = "yyyy-MM-dd'T'HH:mm:ss.SSS"
let kAPI_AppDateFormat                  = "yyyy-MM-dd HH:mm:ss.SSS"
let kAPI_Success                        = "success"
let kAPI_Error                          = "error"
let kAPI_MessageAlert                   = "Message"
let kAPI_Message                        = "message"
let kAPI_Alert                          = ""
let kAPI_Id                             = "id"

let kAPI_ServerNotificationDateFormat   = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
let kAPI_24HourTimeFormat               = "HH:mm:ss"
let kAPI_12HourTimeFormat               = "hh:mm a"


//****************************************************
// MARK: - Api Constant
//****************************************************

let kAPI_Name                           = "name"
let kAPI_Email                          = "email"
let kAPI_Password                       = "password"
let kAPI_ProfilePic                     = "profile_pic"
let kAPI_UserType                       = "user_type" // (customer = 1 & provider=2)
let kAPI_FCMId                          = "fcm_id"
let kAPI_UserId                         = "user_id"
let kAPI_IOSId                          = "ios_id"
let kAPI_UserInfo                       = "userinfo"
let kAPI_Image                          = "image"
let kAPI_ProfileImage                   = "profile_image"
let kAPI_Status                         = "status"
let kAPI_OnlineStatus                   = "online_status"
let kAPI_CreatedAt                      = "created_at"
let kAPI_ForgotToken                    = "forgot_token"
let kAPI_DeviceId                       = "device_id"
let kAPI_ParentId                       = "parent_id"
let kAPI_ServiceStatus                  = "service_status"
let kAPI_ServiceId                      = "service_id"
let kAPI_ServiceName                    = "service_name"
let kAPI_ServiceList                    = "service_list"
let kAPI_PId                            = "pid"
let kAPI_PServiceId                     = "p_service_id"
let kAPI_PAddress                       = "p_address"
let kAPI_Latitude                       = "latitude"
let kAPI_Longitude                      = "longitude"
let kAPI_PTransportId                   = "p_transport_id"
let kAPI_LaberId                        = "laber_id"

//** Pull to refresh
let kAPI_Meta                           = "_meta"
let kAPI_CurrentPage                    = "currentPage"
let kAPI_PageCount                      = "pageCount"
let kAPI_NextPageCount                  = "nextPageCount"
let kAPI_AccessToken                    = "token"
let kAPI_SocialId                       = "social_id"


//** Google Api Key
//let GoogleAPI_Key  = "AIzaSyCyCSU6y1bmONSyRJJDjvrwJI2V_pcR8uQ"
//let GoogleAPI_Key       = "AIzaSyCPE8ly_6R1bsvQjfJEqEwMHjCCkV1Rm0k"
//let GoogleAPI_Key       = "AIzaSyDg2tlPcoqxx2Q2rfjhsAKS-9j0n3JA_a4"
//let GoogleAPI_Key           = "AIzaSyCsGq4KrfXHoszUGlyhKb6jlP6l71i4diM"
//let GoogleAPI_Key           = "AIzaSyDURL4saLzKPtpRMTaB0cUSH8zsgd-TnYk"
// new 2 let GoogleAPI_Key           = "AIzaSyCxF_TQ0w5MCPxFaAq2EiY6zq7tXz-45_4"
//let GoogleAPI_Key           = "AIzaSyBED3MJ4NT5eBHwe1xKPGuar2f6wufgYZk"
//let GoogleAPI_Key           = "AIzaSyCBkAFwoSYvHHASfeGfjTn_P9bHgNF8SFU"
//let GoogleAPI_Key           = "AIzaSyBxWIJILBtGmixfN79rfwa4y6VCzLsO49g"
//AIzaSyBdVl-cTICSwYKrZ95SuvNw7dbMuDt1KG0
//let GoogleAPI_Key           = "AIzaSyAUESuWeONu0IlukkPyYHFg7oZykwW2SqY"
let GoogleAPI_Key           = "AIzaSyC4biOQ0HsRQe1JeeXl6P5QvSAXv85kPAg"
let baseUrl                 = "https://maps.googleapis.com/maps/api/geocode/json"
let baseURLString           = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
let baseUrlLocation         = "http://maps.google.com/maps/api/geocode/json?sensor=false&address="
let TwitterConsumer_Key     = "wXQAImJzvw3HpbalKsj3WnFJD"
let TwitterConsumer_Secret  = "3LvDKhqyzmggnrS0oF1BmwYVHlU83LfBiyRxgQ2xJh22OumxhW"
let NewRelicAPI_Key         = "493244f63b6928dc31403e6d59d24959828a979941bfb45"

//**    Result Success |"1"| / Failure |"0"|
let kAPI_Result_Success                            = "1"
let kAPI_Result_Failure                            = "0"

//** Certificate Type
let kCertificateDevelopment         = "development"
let kCertificateDistribution        = "distribution"
let kCertificateAppStore            = "live"


//** Other Api Constants
let kDevicePlatform                 = "ios"


//let PayPalSandboxKey                = "AU3z5kmOwgRf4tpOa31cenjlwR1gCyZtTSWB0jHIUbRKtafi_XWT19G0i92odt7lU3mIsf9iun5YcjWu"

//let kAPI_PayPal = "AU3z5kmOwgRf4tpOa31cenjlwR1gCyZtTSWB0jHIUbRKtafi_XWT19G0i92odt7lU3mIsf9iun5YcjWu"

//*> Server response code

//    200 – OK – Eyerything is working
//    201 – OK – New resource has been created
//    204 – OK – The resource was successfully deleted
//
//    304 – Not Modified – The client can use cached data
//
//    400 – Bad Request – The request was invalid or cannot be served. The exact error should be explained in the error payload. E.g. „The JSON is not valid“
//    401 – Unauthorized – The request requires an user authentication
//    403 – Forbidden – The server understood the request, but is refusing it or the access is not allowed.
//    404 – Not found – There is no resource behind the URI.
//    422 – Unprocessable Entity – Should be used if the server cannot process the enitity, e.g. if an image cannot be formatted or mandatory fields are missing in the payload.
//
//    500 – Internal Server Error – API developers should avoid this error. If an error occurs in the global catch blog, the stracktrace should be logged and not returned as response.

//** Testing Login credential

/*
1)
//Username  : "kateconnor",
//Password  : "kate123",

2)
// Username: scott-test
//Password: AppTeam2015
*/
