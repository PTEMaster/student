//
//  UITextField+Custom.swift
//  Poshrite
//
//  Created by Neuron on 6/13/16.
//  Copyright © 2016 Codiant. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {

    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
    
    @IBInspectable var paddingLeftCustom: CGFloat {
             get {
                 return leftView!.frame.size.width
             }
             set {
                 let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
                 leftView = paddingView
                 leftViewMode = .always
             }
         }

         @IBInspectable var paddingRightCustom: CGFloat {
             get {
                 return rightView!.frame.size.width
             }
             set {
                 let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
                 rightView = paddingView
                 rightViewMode = .always
             }
         }
}
