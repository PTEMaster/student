//
//  NoRecorFound+UIView.swift
//  Bosala


import Foundation

extension UITableView {
    
    func addNoRecordsView (_ parentView: AnyObject, yAxis: CGFloat, textMessage: NSString) {
        
        if #available(iOS 12.0, *) {
            self.frame              = CGRect(x: 0, y: 0, width: parentView.frame.size.width, height: parentView.frame.size.height)
        } else {
            // Fallback on earlier versions
        }
        
        let label               = UILabel(frame: CGRect(x: 0, y: yAxis, width: self.frame.size.width, height: 40))
        label.center            = CGPoint(x: parentView.center.x, y: parentView.center.y)
        label.textAlignment     = NSTextAlignment.center
        label.textColor         = UIColor.black
        label.numberOfLines     = 2
        label.text              = textMessage as String
        label.tag               = 101
        label.font              = UIFont(name: AppFontMedium, size: 17.0)
        self.addSubview(label)
    }
    
    func EmptyMessage(message:String, viewController:AnyObject) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width:viewController.view.bounds.size.width,  height:viewController.view.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 2;
        messageLabel.textAlignment = NSTextAlignment.center
        messageLabel.font              = UIFont(name: AppFontMedium, size: 17.0)
        messageLabel.tag               = 101
        messageLabel.sizeToFit()
        self.addSubview(messageLabel)
    }
    
    func removeNoRecordView () {
        let subViews = self.subviews
        for subview in subViews {
            if subview.tag == 101 {
                subview.removeFromSuperview()
            }
        }
    }
}


