//
//  TwoDecimalPlace+Double.swift
//  Hoffr
//
//  Created by Neuron on 3/9/16.
//  Copyright © 2016 Neuron Solutions Inc. All rights reserved.
//

import Foundation

public extension Double {
    public func roundTo(_ decimalPlaces: Int) -> Double {
        var v = self
        var divisor = 1.0
        if decimalPlaces > 0 {
            for _ in 1 ... decimalPlaces {
                v *= 10.0
                divisor *= 0.1
            }
        }
        return v.rounded() * divisor
    }
}

//extension Double {
//    /// Rounds the double to decimal places value
//    func roundToPlaces(_ places:Int) -> Double {
//        let divisor = pow(10.0, Double(places))
//        return round(self * divisor) / divisor
//    }
//}
