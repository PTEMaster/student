//
//  SelectNotiCell.swift
//  SwipeStudent
//
//  Created by mac on 28/06/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class SelectNotiCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
