//
//  StudentCell.swift
//  Swipe Admin
//
//  Created by CTInformatics on 12/01/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class StudentCell: UITableViewCell {
 @IBOutlet weak var lblName:UILabel!
@IBOutlet weak var lblID:UILabel!
@IBOutlet weak var lblHR:UILabel!
@IBOutlet weak var lblGR:UILabel!
@IBOutlet weak var btnnext:UIButton!
@IBOutlet weak var imgPhoto:UIImageView!
@IBOutlet weak var viewbackground: UIView!
    
//SheduleData
    @IBOutlet weak var lblSClass:UILabel!
    @IBOutlet weak var lblSRoom:UILabel!
    @IBOutlet weak var lblSPeriod:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
