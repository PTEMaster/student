//
//  PeriodScans.swift
//  SwipeStudent
//
//  Created by mac on 05/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class PeriodScans: NSObject {

//    "Description" : "OTHER: Period 1, Scanned TU",
//    "Name" : "Period 1, Scanned TU",
//    "SchoolId" : 11102495,
//    "ShortFormattedClassDate" : "04\/04\/2018",
//    "LongFormattedClassDate" : "04\/04\/2018 10:16:43 AM",
//    "Source" : "VXNlwq\/R8hkjjoEBCF2X",
//    "SourceId" : 1930911,
//    "ClassDate" : "\/Date(1522851403000-0000)\/",
//    "StudentId" : 34372,
//    "Type" : "OTHER"
    
    var Description             : String?
    var Name                    : String?
    var SchoolId                : String?
    var ShortFormattedClassDate : String?
    var LongFormattedClassDate  : String?
    var Source                  : String?
    var SourceId                : String?
    var ClassDate               : String?
    var StudentId               : String?
    var TypeOne                 : String?
    
    
    /**
     * Initialize Mines Sites object with server data
     */
    init(sites: JSON) {
        
        if let st = sites["Description"].string {
            Description = st
        }
        
        if let st = sites["Name"].string {
            Name = st
        }
        
        if let st = sites["ShortFormattedClassDate"].string {
            ShortFormattedClassDate = st
        }
        
        if let st = sites["LongFormattedClassDate"].string {
            LongFormattedClassDate = st
        }
        
        if let st = sites["Source"].string {
            Source = st
        }
        
        if let st = sites["SourceId"].string {
            SourceId = st
        }
        
        if let st = sites["ClassDate"].string {
            ClassDate = st
        }
        
        if let st = sites["StudentId"].string {
            StudentId = st
        }
        
        if let st = sites["Type"].string {
            TypeOne = st
        }
    }
    
    /**
     * Parse Sites Json data and Return array of Mines objects
     */
    class func getPeriodScans(_ response: AnyObject) -> [PeriodScans] {
        
        //** Json Parsing
        let json = JSON(response)
        
        var arraySites = [PeriodScans]()
        
        let jsonItems = json
        
        for result in jsonItems["data"].arrayValue {
            arraySites.append(PeriodScans(sites: result))
        }
        return arraySites
    }
}
