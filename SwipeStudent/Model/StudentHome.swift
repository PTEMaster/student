//
//  StudentHome.swift
//  SwipeStudent
//
//  Created by mac on 03/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class StudentHome: NSObject {

    /* "DayName" : "E",
     "SectionCode" : "3",
     "Semester_End" : "\/Date(2209006800000-0000)\/",
     "Semester_Name" : "S1",
     "StartTime" : " 6:05 AM",
     "PerName" : "Per 1-E(Y)",
     "ScheduleId" : 51901,
     "RoomName" : "",
     "Semester_Start" : "\/Date(1346040000000-0000)\/",
     "EndTime" : "08:37 AM",
     "ClassName" : "SPANISH III",
     "ClassCode" : "3030300",
     "Teacher" : "LEWIS-HIRALDO, N."*/
    
    var DayName         : String?
    var SectionCode     : String?
    var Semester_End    : String?
    var displayName     : String?
    var Semester_Name   : String?
    var StartTime       : String?
    var PerName         : String?
    var ScheduleId      : String?
    var RoomName        : String?
    var Semester_Start  : String?
    var EndTime         : String?
    var ClassName       : String?
    var ClassCode       : String?
    var Teacher         : String?
    
    /**
     * Initialize Mines Sites object with server data
     */
    init(sites: JSON) {

        if let st = sites["DayName"].string {
            DayName = st
        }
        
        if let st = sites["SectionCode"].string {
            SectionCode = st
        }
        
        if let st = sites["Semester_End"].string {
            Semester_End = st
        }
        
        if let st = sites["displayName"].string {
            displayName = st
        }
        
        if let st = sites["Semester_Name"].string {
            Semester_Name = st
        }
        
        if let st = sites["StartTime"].string {
            StartTime = st
        }
        
        if let st = sites["PerName"].string {
            PerName = st
        }
        
        if let st = sites["ScheduleId"].string {
            ScheduleId = st
        }
        
        if let st = sites["RoomName"].string {
            RoomName = st
        }
        
        if let st = sites["Semester_Start"].string {
            Semester_Start = st
        }
        
        if let st = sites["EndTime"].string {
            EndTime = st
        }
        
        if let st = sites["ClassName"].string {
            ClassName = st
        }
        
        if let st = sites["ClassCode"].string {
            ClassCode = st
        }
        
        if let st = sites["Teacher"].string {
            Teacher = st
        }
    }
    
    /**
     * Parse Sites Json data and Return array of Mines objects
     */
    class func getScheduel(_ response: AnyObject) -> [StudentHome] {
        
        //** Json Parsing
        let json = JSON(response)
        
        var arraySites = [StudentHome]()
        
        let jsonItems = json
        
        for result in jsonItems.arrayValue {
            arraySites.append(StudentHome(sites: result))
        }
        return arraySites
    }
    
}
