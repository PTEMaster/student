//
//  AttendenceDaily.swift
//  SwipeStudent
//
//  Created by mac on 05/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class AttendenceDaily: NSObject {
    
    var AttendanceId    : String?
    var Date            : String?
    var EntryTime       : String?
    var Excuse          : Int?
    var RecordedBy      : String?
    var Excuses         : [JSON]?
    var Options         : [JSON]?
    var IdExc           : String?
    var status          : String?
    

    /**
     * Initialize Mines Sites object with server data
     */
    init(sites: JSON) {
        
        if let st = sites["AttendanceId"].string {
            AttendanceId = st
        }
        
        if let st = sites["Date"].string {
            Date = st
        }
        
        if let st = sites["EntryTime"].string {
            EntryTime = st
        }
        
        if let st = sites["Excuse"].int {
            Excuse = st
        }
        
        if let st = sites["RecordedBy"].string {
            RecordedBy = st
        }
        
        if let st = sites["Status"].string {
            status = st
        }
        
        Excuses = sites["Excuses"].arrayValue
        Options = sites["Options"].arrayValue
    }
    
    /**
     * Parse Sites Json data and Return array of Mines objects
     */
    
    class func getPeriodScans(_ response: AnyObject) -> [AttendenceDaily] {
        
        //** Json Parsing
        let json = JSON(response)
        var arraySites = [AttendenceDaily]()
        let jsonItems = json
        for result in jsonItems["results"].arrayValue {
            arraySites.append(AttendenceDaily(sites: result))
        }
        return arraySites
    }
}
