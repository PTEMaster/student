//
//  Alert.swift
//  SwipeStudent
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class Alert: NSObject {
    
    var LongStartDate    : String?
    var OutcomeType    : String?
    var DisplayText    : String?
    
    /**
     * Initialize Mines Sites object with server data
     */
    init(sites: JSON) {
        
        if let st = sites["LongStartDate"].string {
            LongStartDate = st
        }
        
        if let st = sites["OutcomeType"].string {
            OutcomeType = st
        }
        
        if let st = sites["DisplayText"].string {
            DisplayText = st
        }
    }
    
    /**
     * Parse Sites Json data and Return array of Mines objects
     */
    
    class func getPeriodScans(_ response: AnyObject) -> [Alert] {
        
        //** Json Parsing
        let json = JSON(response)
        var arraySites = [Alert]()
        let jsonItems = json
        for result in jsonItems["data"].arrayValue {
            arraySites.append(Alert(sites: result))
        }
        return arraySites
    }
}


