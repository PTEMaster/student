//
//  SwipeStudent-Bridging-Header.h
//  SwipeStudent
//
//  Created by mac on 17/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

#ifndef SwipeStudent_Bridging_Header_h
#define SwipeStudent_Bridging_Header_h

@import Kingfisher;
@import IQKeyboardManagerSwift;
@import XCGLogger;
@import SwiftyJSON;
@import SmileLock;
@import QRCoder;
@import SwiftPhoneNumberFormatter;
@import AWSSNS;
@import AWSCognito;
@import AWSCognitoAuth;
@import AWSCore;

//#import <AWSCore/AWSCore.h>
//#import <AWSCognito/AWSCognito.h>

#endif /* SwipeStudent_Bridging_Header_h */
